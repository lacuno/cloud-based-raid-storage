import re
import os
import sys

credentials = []
dirs = []
regex = r"^[^%].*=(.*)$"

# Get credential file paths from paths.properties file which is used in
# the source code to get the credentials
try:
    with open("conf/paths.properties") as pathfile:
        data = pathfile.read()

        foundList = re.findall(regex, data, flags=re.MULTILINE)
        for found in foundList:
            dirs.append(found)
except IOError:
    print("conf/paths.properties is not readable!")
    sys.exit(-1)

# Search for credentials in the given directories
for directory in dirs:
    with open (directory, "r") as credentialfile:
        data=credentialfile.read()

        foundList = re.findall(regex, data, flags=re.MULTILINE)
        print("Checking for credentials from " + directory + ":")
        if len(foundList) is 0:
            print("   Warning: No credentials found this StorageProvider")

        for found in foundList:
            credentials.append(found)
            print("   " + found)
        print("\n")

# Go through all files starting from the directory calling the script.
# This part will open every file in every subfolder and search for credentials
foundCredential = False

for root, subFolders, files in os.walk("."):
    for filee in files:
        with open(os.path.join(root, filee), 'r') as fin:
            for line in fin:
                for credential in credentials:
                    if credential in line:
                        foundCredential = True
                        print("FOUND CREDENTIAL IN FILE: " + root + "/"+ filee)

if foundCredential:
    print("DO NOT PUSH!!!")
    sys.exit(-1)

else:
    print("Your files are ready to push")
    sys.exit(0)
