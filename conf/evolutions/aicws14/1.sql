# Users schema

# --- !Ups

CREATE TABLE files
(
  name VARCHAR(50),
  version INT,
  raidlevel INT,
  numberofserversdistributed INT,
  raid5parityserver VARCHAR(50),
  filehash VARCHAR(256),
  PRIMARY KEY(name,version)
);

# --- !Downs

DROP TABLE files;