/**
 * Created by francesco on 14/01/15.
 */

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.*;

import static org.junit.Assert.*;

import app.Modules;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Collection;

import storageProvider.*;

import java.util.concurrent.TimeUnit;

@RunWith(Parameterized.class)
public class StorageProviderTest {

    @Parameters
    public static Collection<Object[]> data() throws Exception {

        InputStream input = new FileInputStream("conf/paths.properties");

        Properties prop = new Properties();

        prop.load(input);

        Enumeration e = prop.propertyNames();

        while (e.hasMoreElements()) {
            String key = (String) e.nextElement();
            Modules.addCredentialFilePath(key, prop.getProperty(key));
        }

        return Arrays.asList(new Object[][]{
                {new DropboxStorageProvider()}, {new AmazonStorageProvider()}, {new BoxStorageProvider()}
        });
    }

    private IStorageProvider sp;
    private String test_filename;
    private String test_data;

    public StorageProviderTest(IStorageProvider sp) {
        this.sp = sp;
        this.test_filename = UUID.randomUUID().toString();
        this.test_data = UUID.randomUUID().toString();
    }

    @Test
    public void notNull() {
        assertNotNull(this.sp);
    }

    @Test
    public void uploadRetrieveAndDelete() {
        try {
            this.sp.postFile(this.test_filename, test_data.getBytes());
        } catch (Exception ex) {
            fail(ex.getMessage());
        }

        try {
            byte[] file = this.sp.getFile(this.test_filename);
            assertArrayEquals(file, this.test_data.getBytes());
        } catch (Exception ex) {
            fail(ex.getMessage());
        }

        try {
            this.sp.deleteFile(this.test_filename);
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }

    @Test
    public void longTest() {
        for (long stop = System.nanoTime() + TimeUnit.SECONDS.toNanos(300); stop > System.nanoTime(); ) {
            String filename = UUID.randomUUID().toString();
            Random randomGenerator = new Random();
            byte[] data = new byte[randomGenerator.nextInt(10000)];
            new Random().nextBytes(data);
            try {
                this.sp.postFile(filename, data);
            } catch (Exception ex) {
                fail(ex.getMessage());
            }

            try {
                byte[] file = this.sp.getFile(filename);
                assertArrayEquals(file, data);
            } catch (Exception ex) {
                fail(ex.getMessage());
            }

            try {
                this.sp.deleteFile(filename);
            } catch (Exception ex) {
                fail(ex.getMessage());
            }
        }
    }
}