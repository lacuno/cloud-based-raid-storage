\documentclass{sig-alternate}

\begin{document}

\title{Modern Cloud Federation Approaches}

\numberofauthors{5}
\author{
% 1st. author
\alignauthor
Christoph Weiler\\
       \email{e1029175@student. tuwien.ac.at}
% 2nd. author
\alignauthor
David Markusfeld\\
       \email{e1125239@student. tuwien.ac.at}
% 3rd. author
\alignauthor Ivan Pryakhin\\
       \email{e1028223@student. tuwien.ac.at}
\and  % use '\and' if you need 'another row' of author names
% 4th. author
\alignauthor Thiago Carneiro \\
       \email{e1028762@student. tuwien.ac.at}
% 5th. author
\alignauthor Francesco Infante \\
       \email{e1428498@student. tuwien.ac.at}
}

\maketitle
\begin{abstract}
Many cloud implementations are based on single cloud storage systems.
But recent events, such as the Amazon S3 multi-hour failure, showed that we need more reliability.
Researchers have addressed this issue by federating clouds in different manners.

In this paper we summarize the state of the art cloud federation approaches.
First we distinguish cloud federation along several dimensions, namely Redundant Deployment, Redundant Computation and Parallel Computation.
Then we discuss the approach of using on-demand services to build a RAID5-cloud system and discuss pros and cons.
Then we address a cloud federation approach that does not aim for availability specifically.
Instead the clouds work together on different layers, such as: Software-as-a-Service, Platform-as-a-Service, etc.
Last we discuss a high-availability cloud implementation named HAIL.
The approach claims to have a very high resilience against attackers and builds a cloud-federation among unreliable components.

We conclude our paper with final thoughts about cloud federation.

\end{abstract}

\section{Introduction}

Cloud computing deals with computation tasks, data access and storage services, in a way that it is not required for the client to know the physical location of the system which delivers the required services.
It enables access to a shared pool of configurable computing resources, servers, storage, applications, and services. However, the benefits of using the cloud computing come at a price, the customer is typically tied closely to just one provider~\cite{metastorage}.

This, implies a high degree of vendor $lock-in$ where the customer not only depends on the proprietary interfaces and APIs specified by the provider but also employs a workforce of specialists for that particular provider who are unlikely to be able to work with a competitors cloud offering~\cite{cloudfederation}.

Cloud Federation is one concept to confront challenges that still persist in Cloud Computing, such as vendor lock-in or compliance requirements.
The lack of a standardized meaning for the term Cloud Federation has led to multiple conflicting definitions and an unclear prospect of its possible benefits.
Cloud Federation has been proposed as a way to address many problems.
So far, there is an abundance of work where federated storage services are used
and different cloud federation strategies have been identified.~\cite{cloudfederation}

This paper is structured as follows: In section~\ref{sec:section2} we describe our first approach for the compute federation strategies.
In section~\ref{sec:section3} we discuss architecture of an application that implements Cloud-RAID approach for multiple storage providers with focus on economy factor.
In the next section ~\ref{sec:section4} we talk about an approach where Cloud services are represented as a vertical stack with three discrete layers: application, platform and infrastructure layer.
Section~\ref{sec:section5} addresses an abstraction layer that works in a RAID-like fashion named HAIL (High-Availability and Integrity Layer).
Finally, we conclude this paper with section~\ref{sec:conclusions}.

\section{Different dimensions model}
\label{sec:section2}
The first approach, we consider to distinguish cloud federation along several dimensions.

\begin{itemize}
  \item Federation can occur within a layer of the technical
cloud computing stack (i.e., as an example to
federate two infrastructure services) or across layers
(e.g., a platform as a service (PaaS) offering combined
with an Infrastructure as a Service (IaaS) offering).

  \item For federation within the IaaS layer there are two main
kinds of services: compute and storage.

  \item Cloud federation can happen for an unlimited (at least
regarding a near time horizon) or a limited period
of time. The first case is typically called redundancy
while the latter is called migration~\cite{cloudeffects}.

\end{itemize}

This approach focus on the redundant usage of compute resources, i.e., on permanent federation of services within the IaaS layer. It uses five strategies the redundant usage of compute services.

The first strategy is $Redundant$ $Deployment$: the goal of this approach is to remove
the single point of failure which exists in a scenario that uses only one provider. 
It is likely to improve response times due to increased client proximity of the deployments.
Another motivation can be compliance with laws and regulations which sometimes require to keep sensitive data within a country. It offers a very high level of availability and an excellent
resilience to failures not caused by the application code itself.

The second strategy is $Redundant$ $Computation$: the goal is that every request is forwarded to all clouds. Depending on the goal of this strategy, we can then either wait for all results and compare them (RC with Comparison) or use the first result which is returned
(RC without Comparison). 

The RC with Comparison provides rather poor availability, the more participating clouds, the lower the availability. The probability of processing a particular request in cloud $c$ is 100\%  The distribution of resulting processing times is basically a convolution of all participating clouds’ processing time distributions. 

The RC without Comparison scenario provides excellent availability and performance, the more participating clouds, the better availability and response times. The distribution of resulting processing times. This leads to high monetary costs as all requests are processed in all clouds.

The third strategy is $Parallel$ $Computation$ (PC): There are two main goals for this scenario: First, for sensitive data, processing only fragments guarantees that a malevolent cloud provider sees only parts of the data.

Second, a fragment is smaller than the original data so that processing becomes faster and PC can, hence, be expected to complete faster than all other strategies. But not every request can be processed on fragments of data. The distribution of the resulting processing time is again a convolution of the participating clouds processing times using the max operator and is, hence, vulnerable to hung requests.

\section{Cloud-RAID storage approach}
\label{sec:section3}

Authors in \cite{impl} discuss an application that utilizes advantages of Cloud Computing, namely on-demand service, i.e. a customer pays only for resources he/she used. That brings a lot of economic benefits to the end user, providing other positive aspects, such as reliability, confidentiality and security of data. Architecture and design of such application is discussed in that paper.

As mentioned in the previous section, when a customer depends on one cloud storage provider only, vendor $lock-in$ is possible. Usually a customer pays for inbound and outbound traffic, meaning that if one wants to change storage provider, he/she must pay a lot of money for data to be transferred. Therefore a customer depends on inbound and outbound rates of a single provider.

Application described in \cite{impl} solves this problem as well as other challenges that appear with a single storage provider, in particular:
\begin{itemize}
\item \textbf{Security}. Although many cloud storage providers do guarantee safety of data, a customer cannot rely on such promises. Customers' data may be secure from outside Internet, but provider still has physical access to servers where data is hosted, which is $a priori$ a security disadvantage. One way to solve this problem is to encrypt the data before sending it to the cloud, but that requires management of cryptographic keys by a user, which is not only time consuming but has usability issues as well. With cloud based distribution of data this problem is solved by storing parts of file in different storage providers and encrypting each part. This way any single storage provider does not have access to all customers' data.

\item \textbf{Availability}. Besides security risks, there are availability concerns when storing data in single storage provider. There are events that cannot be controlled or predicted that make the data unavailable. For example, in case of hardware failure data will be lost with little chance of recovering. A company providing storage space may also close down or go out of business (due to economical reasons for example). Cloud RAID based approach solves this problem again by using multiple storage vendors, where a failure of one (or more) providers may be tolerated. This also solves the issue of reliability of data, because data may get corrupted and deemed as unusable.

\end{itemize}

Now that we have established aspects that have to be taken into account, we can give a general architecture of the application. As stated in the beginning of this section the main focus lies in the economical benefits of the Cloud based services. RAID like strategy is implemented using erasure coding techniques. In \cite{impl} authors have implemented three components, namely:
\begin{itemize}
\item \textbf{User Interface}. This is a rather self-explanatory module, that provides user interaction with the application. It gives overview of the data, storage providers, settings and enables user to manage them.

\item \textbf{Resource Management}. This module is the service layer of the application. It manages splitting data into different parts using algorithms that take user settings into account.

\item \textbf{Data Management}. This module is the layer between the resource management module and actual storage providers. 
\end{itemize}

As stated above, user interface provides overview of the data and enables user to set certain settings. In particular, user is able to choose between different strategies of data deployment (content deployment or CD). There is $Budget-oriented$ CD that focuses on providers' costs, there is a strategy that chooses providers based on their quality of service parameters and there is a strategy that stores user data based on geographical location of servers (that is used mostly in cases where data is required to be hosted in country by law).

Management of storage repositories has six abstract functions that are then implemented for each provider specifically, namely: $create$ for creating a new container, $write$ for writing data into container, $read$, $list$, $delete$ and $digest$ for getting hash value of an object. Container is an abstract object that simply represents a classical computer folder, but for the purposes of abstraction is named that way. 

Besides six functions for data manipulation, management of repositories component stores information about capabilities and other quality of service parameters about storage providers which are then used when choosing different strategies for data deployment.

Application uses the same data model as the one used by the Amazon S3 Services, i.e. container's name must be unique and every object is addressed by container name and object key. 

As stated above RAID strategy is implemented using erasure code techniques. That means that $n$ storage providers are divided into two groups $n=k+m$, where $k$ and $m$ are variable parameters (based on user preference). Each file is split into $k$ packages (data chunks) and $m$ packages are generated based on available information. For example, in classical RAID 5 (with 3 hard drives) $k$ will be $2$ and $m$ will be $1$, because the actual data is split into two parts and the third part (parity) is calculated. Therefore, only a subset of all $n$ parts is needed to reconstruct data. Since modern storage providers claim to have 99\% (or above) availability, $m$ is chosen to be $1$ in this application. That means failure of (any) one storage provider is tolerated. Since more information generated than there originally was (i.e. $n$ parts are physically stored where as actual data is stored in $k$ parts), there will be some overhead. But overhead caused by data encryption is stated to be less than 2\% on average.

To address the performance issue further, it has to be said, that application uses one thread per provider, meaning that all storage provider accesses are independent from each other (it terms of threads). That also means that encryption and decryption of a package (part of a file) is also done in parallel.

In a classical RAID, every disk (or storing entity) is treated equally, i.e. only disk capacity is stored by the data distribution component. Since this application focuses on economical benefits, each storage provider has a $Reputation$ object stored, that contains meta information such as inbound/outbound costs, capacity, (average) availability, performance, etc. This meta information can be used to generate prioritized list of storage providers. Based on user demands this list will be ordered accordingly, e.g. response-time prioritized list may be different than cost optimal list. When reassembling data (read request), only $k$ storage providers needed to be accessed from that list.  

\section{Layered Service Model}
\label{sec:section4}
In this approach, \cite{cloudfederation}, each independent Cloud supports a vertical stack of service layer. At the top we have the Application Layer (SaaS), then the Platform Layer (PaaS) and at the end the operating systems and Infrastructure Layer (IaaS). At each layer we make a choice. We can use $delegation$ and this means that we fulfill a service through local resources, or we can use $federation$. The key feature of this approach is that the choice to use federation is taken at each layer of the service stack. This way at each stack level we allow elasticity and fault-tolerant behavior. Also, this could does not require Cloud providers to implement the full layer stack and allows them to focus on a single layer.

For example, a user submits a request to the application layer of an independent Cloud. If there are sufficient local resources, the SaaS layer will assess them to fulfill the request. If the application layer cannot meet its service requirements, it can use $federation$ and fulfill the request through a SaaS layer provider of another Cloud. The result will return to the user as if it was done locally on the first Cloud. However, the application layer has also the possibility of using $delegation$. That means that it asks his own PaaS layer for more resources. Again, the local PaaS layer can issue a request to his own IaaS layer for others VM or, if this is not possible, through $federation$ asks to another Cloud's PaaS layer to acquire its resources.

This approach brings different challenges. One of them is introducing a standardized form of expressing inter-layer mapping. Also, we have to choose which among $delegation$ and $federation$ to use if both options are available. Since this choice is taken at each layer, we have to introduce metrics that the SaaS layer can understand without revealing information about the underlying infrastructure. 

Brokering at each layer has different purposes. At the SaaS layer it is mainly focused on the user's requirements  and the SLA. Each SaaS layer should guarantee a set of requirements, response time and cost for example. A given SaaS will forward a request to another Cloud provider if it cannot fulfill it at all. However, even if it could fulfill it, it may want to negotiate it with other SaaS. Negotiation is based on information of past history between two Clouds. For example, Cloud A is more cost-effective in general, but it is more energy efficient to give the request to Cloud B if that could allow to switch down servers for Cloud A. Another factor to be taken into account is the Cloud reputation, i.e. how many times were SLA requirements violated.

Brokering at the PaaS layer is based on requirements in terms of deployment and run-time support (libraries). Brokering at this level should increase the performance since we could provide more specialized compilers and execution environments for each request.
Also security and fault tolerance issues may be taken into account when choosing a given execution environment.

Brokering at the IaaS layer can be decomposed into two aspects. The first one, resource provisioning, is responsible to take from a wide array of resources class with different capabilities the best mix and the appropriate number of nodes of a given class to fulfill the requirements of the application. Resource adaptation, instead, is responsible at run-time to adapt dynamically the resources to the requirements. For example, to provide more CPU to a VM to speed up the application or to migrate the VM to another Cloud provider.
Brokering at this level brings advantages from different domains. We can reduce the application time using additional level of parallelism or, also, we can reduce the cost choosing the best mix of resources. It will be much more resilient to unexpected downtime, since we can allocate resources from different Cloud provider. Last but not least, we can optimize the energy efficiency of all Clouds.

\section{HAIL}
\label{sec:section5}
HAIL (High-Availability and Integrity Layer) was introduced by Bowers et al. in 2008 and is a RAID-like cloud federation system designed for maximal availability~\cite{hail}.
The main goal of HAIL is to build a reliable but also cost effective cloud storage service out of unreliable components.
The authors in~\cite{hail} compare their approach to RAID because it builds a reliable storage out of unreliable, independent hard drives.

In HAIL a file $F$ is distributed to $n$ storage providers (e.g. Amazon S3, Dropbox etc.): $S_1, S_2, \dots, S_n$.
This data first is sent to a central element, the gateway, which encodes all data and sends it to the storage providers.
HAIL is assuming that an intruder can corrupt all storage providers over time but in a single epoch (a predefined time-window) it is only possible to corrupt $b$ of of $n$ servers, for some $b < n$.

To protect against a storage provider failure, the data is split into fixed size blocks on so-called primary servers and so-called secondary servers hold a parity information which is calculated from the primary servers' data.
It is done in a RAID5-fashion but it is possible to set more than one parity server.
Bowers et al.~\cite{hail} found out that it is possible to combine the parity information with a message authentication code (MAC) to ensure also data integrity.
This new, combined code is called $dispersal code$.

Because of the assumption that an attacker can modify any server this is not enough.
An attacker could corrupt $b$ servers every epoch and after $\lceil n / b \rceil$ epochs all servers would be corrupted.
Therefore the authors added another encoding layer for each server.
At the end of each file that is saved, a dispersal code is added that ensures integrity of each server. This code is called server-code.
Even the server-code has some parity information to recover potential damage.
Servers which hold parity information also get a server-code to ensure the integrity of the parity and therefore the integrity of error-recovery by using this parity information.

This is still not enough and therefore the gateway has to audit the cloud.
Periodically it sends out challenges with a random row index $i$ to each storage provider and verifies the correctness.
The gateway now recalculates the dispersal code and verifies that the new calculated value is equal to the original dispersal code.
In literature this approach is called proofs of retrievability (PoR)~\cite{prove}.
It is used to prove to a client that all stored data is still available with a very high probability.
For performance- and security amplification it is possible to send multiple requests at once.

If a damaged block is detected it can be reconstructed using the distributed dispersal code and the server-code of the damaged server.
Recovery is very expensive in this configuration, but in reality recovery will be triggered very rarely.

At the moment HAIL only supports static files and there is no update mechanism included in the system.
In the future the authors will work on HAIL providing update mechanisms.

\section{Conclusions}
\label{sec:conclusions}
In this paper we discussed different approaches and architectures that solve some of the cloud service challenges. The first problem when using single cloud service provider is data lock-in, that is when a customer is dependent solely on one providers' resources. That means if this provider goes out of business there is no way to restore users' data. There are also concerns with security of data. Since provider has a physical access to the data, it may not be secure to store it, in that way. Availability is also an issue in this area. Using multiple cloud storage providers with appropriate strategies one can increase data availability.

Cloud Federation is one way to solve all these problems. In the first discussed approach cloud federation is viewed along different dimensions. The second approach focuses on economical aspect of storage providers and discusses RAID strategy using erasure code techniques. Section \ref{sec:section4} describes another approach where cloud services are represented in different layers, where decision has to be made on each one of them - either $delegation$ or $federation$ strategy has to be chosen. In the last section we discussed High-Availability and Integrity Layer strategy that focuses mainly on availability and reliability.

\bibliographystyle{abbrv}
\bibliography{paper}

\end{document}
