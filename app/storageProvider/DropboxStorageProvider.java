
package storageProvider;

import app.Modules;
import com.dropbox.core.DbxClient;
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.DbxWriteMode;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @author Francesco
 */
public class DropboxStorageProvider implements IStorageProvider {

    private final String user_name = "cloudraid2014@gmail.com";

    private List<String> credentials;

    private DbxRequestConfig dropboxConfig;
    //private DbxClient client;

    public DropboxStorageProvider() throws StorageProviderException {
        credentials = Modules.getCredentialProperty("dropbox", new ArrayList<String>() {{
            add("access_token");
        }});
    }

    private DbxClient initializeClient() throws StorageProviderException {
        if(this.credentials == null) {
            throw new StorageProviderException("Credential file is missing for dropbox");
        }
        return new DbxClient(new DbxRequestConfig("DropboxStorageProvider/1.0", Locale.getDefault().toString()), credentials.get(0));
    }

    @Override
    public boolean isOnline() {
        try {
            initializeClient().getAccountInfo();
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    public byte[] getFile(String filepath) throws StorageProviderException {
        if (filepath == null) {
            throw new StorageProviderException("filepath can't be null");
        }

        DbxClient client = initializeClient();

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        try {
            client.getFile("/" + filepath, null, outputStream);
            return outputStream.toByteArray();
        } catch (DbxException e) {
            throw new StorageProviderException(e.toString());
        } catch (IOException e) {
            throw new StorageProviderException(e.toString());
        } finally {
            try {
                outputStream.close();
            } catch (IOException e) {
            }
        }
    }

    @Override
    public void deleteFile(String filepath) throws StorageProviderException {
        if (filepath == null) {
            throw new StorageProviderException("filepath can't be null");
        }

        DbxClient client = initializeClient();

        try {
            client.delete("/" + filepath);
        } catch (DbxException e) {
            throw new StorageProviderException(e.toString());
        }
    }

    @Override
    public void postFile(String filepath, byte[] filedata) throws StorageProviderException {
        if (filepath == null) {
            throw new StorageProviderException("filepath can't be null");
        }
        if (filedata == null) {
            throw new StorageProviderException("filedata can't be null");
        }

        DbxClient client = initializeClient();

        ByteArrayInputStream inputStream = new ByteArrayInputStream(filedata);

        try {
            client.uploadFile("/" + filepath, DbxWriteMode.force(), filedata.length, inputStream);
        } catch (DbxException e) {
            throw new StorageProviderException(e.toString());
        } catch (IOException e) {
            throw new StorageProviderException(e.toString());
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
            }
        }

    }

    @Override
    public String getUniqueIdentifier() {
        return "Dropbox:" + user_name;
    }
}
