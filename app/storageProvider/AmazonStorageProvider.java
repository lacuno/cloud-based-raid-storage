package storageProvider;

import app.Modules;
import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.util.IOUtils;
import play.Logger;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ivan
 */
public class AmazonStorageProvider implements IStorageProvider {

    private static final String BUCKET_NAME = "g4-t2";

    private List<String> credentials;

    public AmazonStorageProvider() {
        this.credentials = Modules.getCredentialProperty("amazon",
                new ArrayList<String>() {{
                    add("accessKey");
                    add("secretKey");
                }});
    }

    private AmazonS3Client initializeClient() throws StorageProviderException{
        if(this.credentials == null) {
            throw new StorageProviderException("Credential file is missing for amazon");
        }
        return new AmazonS3Client(new BasicAWSCredentials(credentials.get(0), credentials.get(1)));
    }

    @Override
    public boolean isOnline() {
        try {
            initializeClient().listObjects(BUCKET_NAME);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    public byte[] getFile(String filepath) throws StorageProviderException {
        AmazonS3 s3client = initializeClient();

        try {
            GetObjectRequest request = new GetObjectRequest(BUCKET_NAME, filepath);
            S3Object object = s3client.getObject(request);
            S3ObjectInputStream objectContent = object.getObjectContent();
            try {
                return IOUtils.toByteArray(objectContent);
            } catch (IOException e) {
                Logger.error(e.getMessage());
	            throw new StorageProviderException(e);
            }
            //FileCopyUtils.copy(objectContent, new FileOutputStream("PATH_TO_SAVE_FILE"));
        } catch (AmazonServiceException ase) {
            if(ase.getErrorCode().equals("NoSuchKey")) {
                return new byte[0];
            }

            Logger.error("Error Message:    " + ase.getMessage());
            Logger.error("HTTP Status Code: " + ase.getStatusCode());
            Logger.error("AWS Error Code:   " + ase.getErrorCode());
            Logger.error("Error Type:       " + ase.getErrorType());
            Logger.error("Request ID:       " + ase.getRequestId());
	        throw new StorageProviderException(ase);
        } catch (AmazonClientException ace) {
            Logger.error("Error Message: " + ace.getMessage());
	        throw new StorageProviderException(ace);
        }
    }

    @Override
    public void deleteFile(String filepath) throws StorageProviderException {
        AmazonS3 s3client = initializeClient();

        try {
            s3client.deleteObject(new DeleteObjectRequest(BUCKET_NAME, filepath));
        } catch (AmazonServiceException ase) {
            Logger.error("Error Message:    " + ase.getMessage());
            Logger.error("HTTP Status Code: " + ase.getStatusCode());
            Logger.error("AWS Error Code:   " + ase.getErrorCode());
            Logger.error("Error Type:       " + ase.getErrorType());
            Logger.error("Request ID:       " + ase.getRequestId());
	        throw new StorageProviderException(ase);
        } catch (AmazonClientException ace) {
            Logger.error("Error Message: " + ace.getMessage());
	        throw new StorageProviderException(ace);
        }
    }

    @Override
    public void postFile(String filepath, byte[] filedata) throws StorageProviderException {
        AmazonS3 s3client = initializeClient();

        try {
            InputStream stream = new ByteArrayInputStream(filedata);
            ObjectMetadata meta = new ObjectMetadata();
            meta.setContentLength(filedata.length);
            //meta.setContentType("application/pdf");
            s3client.putObject(BUCKET_NAME, filepath, stream, meta);

            /*File file = new File(filepath);
            s3client.putObject(new PutObjectRequest(BUCKET_NAME, filepath, file));*/

        } catch (AmazonServiceException ase) {
            Logger.error("Caught an AmazonServiceException.");
            Logger.error("Error Message:    " + ase.getMessage());
            Logger.error("HTTP Status Code: " + ase.getStatusCode());
            Logger.error("AWS Error Code:   " + ase.getErrorCode());
            Logger.error("Error Type:       " + ase.getErrorType());
            Logger.error("Request ID:       " + ase.getRequestId());
	        throw new StorageProviderException(ase);
        } catch (AmazonClientException ace) {
            Logger.error("Error Message: " + ace.getMessage());
	        throw new StorageProviderException(ace);
        }
    }

    @Override
    public String getUniqueIdentifier() {
        return this.getClass().getSimpleName();
    }

    /*
    public static String getAllFiles() {
        AmazonS3 s3client = initializeClient();

        String output = "";


        try {

            ListObjectsRequest listObjectsRequest = new ListObjectsRequest().withBucketName(BUCKET_NAME);
            ObjectListing objectListing;
            do {
                objectListing = s3client.listObjects(listObjectsRequest);
                for (S3ObjectSummary objectSummary :
                        objectListing.getObjectSummaries()) {
                    output += " - " + objectSummary.getKey() + "  " +
                            "(size = " + objectSummary.getSize() +
                            ")\n";
                }
                listObjectsRequest.setMarker(objectListing.getNextMarker());
            } while (objectListing.isTruncated());
        } catch (AmazonServiceException ase) {
            Logger.error("Caught an AmazonServiceException.");
            Logger.error("Error Message:    " + ase.getMessage());
            Logger.error("HTTP Status Code: " + ase.getStatusCode());
            Logger.error("AWS Error Code:   " + ase.getErrorCode());
            Logger.error("Error Type:       " + ase.getErrorType());
            Logger.error("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            Logger.error("Error Message: " + ace.getMessage());
        }

        return output;
    }
*/
}
