package storageProvider;

import org.jdeferred.Deferred;

import java.util.Date;

/**
 * @author  David
 */
public class AsyncOperation {

    private OperationType _operationType;

    private String _filePath;

    private byte[] _fileData;

    private Date _requestDateTime;

    private Deferred<Boolean, StorageProviderException, Double> _uploadDeleteCallback;

    private Deferred<byte[], StorageProviderException, Double> _downloadCallback;

    public AsyncOperation(OperationType _operationType, String _filePath, Date _requestDateTime, Deferred<byte[], StorageProviderException, Double> _downloadCallback) {
        this._operationType = _operationType;
        this._filePath = _filePath;
        this._requestDateTime = _requestDateTime;
        this._downloadCallback = _downloadCallback;
    }

    public AsyncOperation(OperationType _operationType, String _filePath, byte[] _fileData, Date _requestDateTime, Deferred<Boolean, StorageProviderException, Double> _uploadDeleteCallback) {
        this._operationType = _operationType;
        this._filePath = _filePath;
        this._fileData = _fileData;
        this._requestDateTime = _requestDateTime;
        this._uploadDeleteCallback = _uploadDeleteCallback;
    }


    public OperationType getOperationType() {
        return _operationType;
    }

    public void setOperationType(OperationType _operationType) {
        this._operationType = _operationType;
    }

    public String getFilePath() {
        return _filePath;
    }

    public void setFilePath(String _filePath) {
        this._filePath = _filePath;
    }

    public Date getRequestDateTime() {
        return _requestDateTime;
    }

    public void setRequestDateTime(Date _requestDateTime) {
        this._requestDateTime = _requestDateTime;
    }

    public Deferred<byte[], StorageProviderException, Double> getDownloadCallback() {
        return _downloadCallback;
    }

    public void setDownloadCallback(Deferred<byte[], StorageProviderException, Double> _downloadCallback) {
        this._downloadCallback = _downloadCallback;
    }

    public byte[] getFileData() {
        return _fileData;
    }

    public void setFileData(byte[] _fileData) {
        this._fileData = _fileData;
    }

    public Deferred<Boolean, StorageProviderException, Double> getUploadDeleteCallback() {
        return _uploadDeleteCallback;
    }

    public void setUploadDeleteCallback(Deferred<Boolean, StorageProviderException, Double> _uploadDeleteCallback) {
        this._uploadDeleteCallback = _uploadDeleteCallback;
    }

    public enum OperationType {
        upload, download, delete
    }
}
