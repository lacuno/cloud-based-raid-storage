package storageProvider;

import org.jdeferred.Promise;
import org.jdeferred.impl.DeferredObject;

import java.util.Date;
import java.util.LinkedList;
import java.util.Queue;

/**
 *  @author David.
 */
public class QueuedStorageProvider implements IAsyncStorageProvider {

    private IStorageProvider _storageProvider;
    private Queue<AsyncOperation> _queue = new LinkedList<AsyncOperation>();
    private Thread _schedulerThread;


    public QueuedStorageProvider(IStorageProvider storageProvider) {
        this._storageProvider = storageProvider;
    }

    @Override
    public Promise<byte[], StorageProviderException, Double> getFileAsync(String filepath) throws StorageProviderException {
        if (filepath == null)
            throw new StorageProviderException("filepath must not be null");

        DeferredObject<byte[], StorageProviderException, Double> defer = new DeferredObject<byte[], StorageProviderException, Double>();
        _queue.add(new AsyncOperation(AsyncOperation.OperationType.download, filepath, new Date(), defer));

        if (this._schedulerThread == null || !this._schedulerThread.isAlive()) {
            this._schedulerThread = new Thread(new Scheduler());
            this._schedulerThread.start();
        }


        return defer.promise();
    }

    @Override
    public Promise<Boolean, StorageProviderException, Double> deleteFileAsync(String filepath) throws StorageProviderException {

        if (filepath == null)
            throw new StorageProviderException("filepath must not be null");

        DeferredObject<Boolean, StorageProviderException, Double> defer = new DeferredObject<Boolean, StorageProviderException, Double>();
        _queue.add(new AsyncOperation(AsyncOperation.OperationType.delete, filepath, null, new Date(), defer));

        if (this._schedulerThread == null || !this._schedulerThread.isAlive()) {
            this._schedulerThread = new Thread(new Scheduler());
            this._schedulerThread.start();
        }

        return defer.promise();
    }

    @Override
    public Promise<Boolean, StorageProviderException, Double> postFileAsync(String filepath, byte[] filedata) throws StorageProviderException {


        if (filepath == null)
            throw new StorageProviderException("filepath must not be null");

        DeferredObject<Boolean, StorageProviderException, Double> defer = new DeferredObject<Boolean, StorageProviderException, Double>();
        _queue.add(new AsyncOperation(AsyncOperation.OperationType.upload, filepath, filedata, new Date(), defer));

        if (this._schedulerThread == null || !this._schedulerThread.isAlive()) {
            this._schedulerThread = new Thread(new Scheduler());
            this._schedulerThread.start();
        }

        return defer.promise();

    }

    @Override
    public byte[] getFile(String filepath) throws StorageProviderException {
        return _storageProvider.getFile(filepath);
    }

    @Override
    public void deleteFile(String filepath) throws StorageProviderException {
        _storageProvider.deleteFile(filepath);
    }

    @Override
    public void postFile(String filepath, byte[] filedata) throws StorageProviderException {
        _storageProvider.postFile(filepath, filedata);
    }

    @Override
    public boolean isOnline() {
        return _storageProvider.isOnline();
    }

    @Override
    public String getUniqueIdentifier() {
        return _storageProvider.getUniqueIdentifier();
    }

    private class Scheduler implements Runnable {
        @Override
        public void run() {

            while (!_queue.isEmpty()) {

                AsyncOperation element = _queue.poll();
                switch (element.getOperationType()) {
                    case download:

                        try {
                            element.getDownloadCallback().resolve(_storageProvider.getFile(element.getFilePath()));
                        } catch (StorageProviderException e) {
                            element.getDownloadCallback().reject(e);
                        }

                        break;

                    case upload:

                        try {
                            _storageProvider.postFile(element.getFilePath(), element.getFileData());
                            element.getUploadDeleteCallback().resolve(true);
                        } catch (StorageProviderException e) {
                            element.getUploadDeleteCallback().reject(e);
                        }

                        break;

                    case delete:

                        try {
                            _storageProvider.deleteFile(element.getFilePath());
                            element.getUploadDeleteCallback().resolve(true);
                        } catch (StorageProviderException e) {
                            element.getUploadDeleteCallback().reject(e);
                        }

                        break;
                }

            }

        }
    }
}
