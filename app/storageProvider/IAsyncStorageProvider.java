package storageProvider;

import org.jdeferred.Promise;

/**
 * @author David
 */
public interface IAsyncStorageProvider extends IStorageProvider {
    /**
     * request the specified file from the storage provider.
     *
     * @param filepath
     * @return byte[] file data
     * @throws StorageProviderException
     */
    Promise<byte[], StorageProviderException, Double> getFileAsync(String filepath) throws StorageProviderException;

    /**
     * deletes the specified file from the storage provider. If file isn't available method is going to terminate
     *
     * @param filepath
     * @throws StorageProviderException
     */
    Promise<Boolean, StorageProviderException, Double> deleteFileAsync(String filepath) throws StorageProviderException;

    /**
     * creates a new file on the storage providers space and saves the filedata into it
     *
     * @param filepath
     * @throws StorageProviderException
     */
    Promise<Boolean, StorageProviderException, Double> postFileAsync(String filepath, byte[] filedata) throws StorageProviderException;

    /**
     * getUniqueIdentifier returns a unique identifier for the storage provider. The concation is
     * provider-name:user-credential-name
     *
     * @return String unique identifier
     */
    String getUniqueIdentifier();
}
