package storageProvider;

import org.jdeferred.DeferredManager;
import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;
import org.jdeferred.Promise;
import org.jdeferred.impl.DefaultDeferredManager;
import play.Logger;

import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author David.
 */
@Singleton
public class QueuedStorageProviderController implements IStorageProviderController {

    private List<QueuedStorageProvider> storageProviderList = new ArrayList<QueuedStorageProvider>();

    private DeferredManager deferredManager = new DefaultDeferredManager();

    public QueuedStorageProviderController() {

        AmazonStorageProvider asp = new AmazonStorageProvider();
        storageProviderList.add(new QueuedStorageProvider(asp));

        try {
            BoxStorageProvider box = new BoxStorageProvider();
            storageProviderList.add(new QueuedStorageProvider(box));
        } catch (StorageProviderException e) {
            Logger.error("I couldn't instantiate box storage provider");
        }

        try {
            DropboxStorageProvider dsp = new DropboxStorageProvider();
            storageProviderList.add(new QueuedStorageProvider(dsp));
        } catch (StorageProviderException ex) {
            Logger.error("I couldn't instantiate dropbox storage provider");
        }

    }

    @Override
    public List<String> getConnectedStorageProviders() {
        List<String> connectedStorageProviderIDs = new ArrayList<String>();
        for (QueuedStorageProvider storageProvider : storageProviderList) {
            connectedStorageProviderIDs.add(storageProvider.getUniqueIdentifier());
        }

        return connectedStorageProviderIDs;
    }

    @Override
    public boolean sendDataToServer(String filename, byte[] data, String storageProvider) {

        if (filename == null || data == null || storageProvider == null) return false;
        for (IStorageProvider sp : storageProviderList) {
            if (sp.getUniqueIdentifier().equals(storageProvider)) {
                try {
                    sp.postFile(filename, data);
                    return true;
                } catch (StorageProviderException ex) {
                    Logger.error("I could't send data to " + storageProvider);
                    return false;
                }
            }
        }
        return false;

    }

    @Override
    public List<IStorageProvider> sendDataToServer(String filename, byte[] data) {

        if (filename == null || data == null)
            return null;

        final List<Promise<Boolean, StorageProviderException, Double>> defers = new ArrayList<>();
        final List<IStorageProvider> result = new ArrayList<>();

        for (final QueuedStorageProvider storageProvider : storageProviderList) {
            final Promise<Boolean, StorageProviderException, Double> promise;
            try {
                promise = storageProvider.postFileAsync(filename, data);
            } catch (StorageProviderException e) {
                return null;
            }

            promise.done(new DoneCallback<Boolean>() {
                @Override
                public void onDone(Boolean data) {
                    System.out.println("check from " + storageProvider.getUniqueIdentifier());
                    result.add(storageProvider);
                    defers.remove(promise);
                }
            }).fail(new FailCallback<StorageProviderException>() {
                @Override
                public void onFail(StorageProviderException e) {
                    System.out.println(e.getMessage());
                    e.fillInStackTrace();
                    defers.remove(promise);
                }
            });
            defers.add(promise);

        }

        while (!defers.isEmpty()) {
            try {
                Thread.currentThread().sleep(500);
            } catch (InterruptedException e) {
                Logger.error("thread interrupted while sleeping");
            }
        }

        return result;
    }

    @Override
    public byte[] getDataFromServer(String filename, final String storageProvider) {

        if (filename == null || storageProvider == null) return null;
        for (IStorageProvider sp : storageProviderList) {
            if (sp.getUniqueIdentifier().equals(storageProvider)) {
                try {
                    return sp.getFile(filename);
                } catch (StorageProviderException ex) {
                    Logger.error("I could't retrieve data from " + storageProvider);
                }
            }
        }
        return null;
    }

    @Override
    public Map<String, byte[]> getDataFromServer(String filename) {

        if (filename == null)
            return null;

        final Map<String, byte[]> result = new HashMap<>();
        final List<Promise<byte[], StorageProviderException, Double>> defers = new ArrayList<>();

        for (final QueuedStorageProvider storageProvider : storageProviderList) {
            final Promise<byte[], StorageProviderException, Double> promise;
            try {
                promise = storageProvider.getFileAsync(filename);
            } catch (StorageProviderException e) {
                return null;
            }

            promise.done(new DoneCallback<byte[]>() {
                @Override
                public void onDone(byte[] data) {
                    System.out.println("check from " + storageProvider.getUniqueIdentifier());
                    result.put(storageProvider.getUniqueIdentifier(), data);
                    defers.remove(promise);
                }
            }).fail(new FailCallback<StorageProviderException>() {
                @Override
                public void onFail(StorageProviderException e) {
                    System.out.println(e.getMessage());
                    result.put(storageProvider.getUniqueIdentifier(), null);
                    defers.remove(promise);
                }
            });
            defers.add(promise);

        }

        while (!defers.isEmpty()) {
            try {
                Thread.currentThread().sleep(500);
            } catch (InterruptedException e) {
                Logger.error("thread interrupted while sleeping");
            }
        }

        return result;
    }

    @Override
    public void deleteDataFromServer(String filename) {

        if (filename == null)
            return;

        for (final QueuedStorageProvider storageProvider : storageProviderList) {
            Promise<Boolean, StorageProviderException, Double> promise = null;
            try {
                promise = storageProvider.deleteFileAsync(filename);
            } catch (StorageProviderException e) {
                System.out.println("couldn't delete from " + storageProvider.getUniqueIdentifier());
            }

            if (promise != null) {
                promise.done(new DoneCallback<Boolean>() {
                    @Override
                    public void onDone(Boolean data) {
                        System.out.println("check from " + storageProvider.getUniqueIdentifier());
                    }
                }).fail(new FailCallback<StorageProviderException>() {
                    @Override
                    public void onFail(StorageProviderException e) {
                        System.out.println(e.getMessage());
                    }
                });

            }
        }

    }

    @Override
    public void deleteDataFromServer(String filename, String storageProvider) {
        if (filename == null || storageProvider == null) return;
        for (IStorageProvider sp : storageProviderList) {
            if (sp.getUniqueIdentifier().equals(storageProvider)) {
                try {
                    sp.deleteFile(filename);
                } catch (StorageProviderException ex) {
                    Logger.error("I could't delete data from " + sp.getUniqueIdentifier());
                }
            }
        }
    }

    @Override
    public Map<String, Boolean> getServerStatus() {
        Map<String, Boolean> map = new HashMap<>();
        for (IStorageProvider sp : storageProviderList) {
            map.put(sp.getUniqueIdentifier(), sp.isOnline());
        }
        return map;
    }
}
