package storageProvider;

import java.util.List;
import java.util.Map;

/**
 * @author David.
 */
public interface IStorageProviderController {

    /***
     * getConnectedStorageProviders returns a list of the currently connected StorageProviders' unique identifier
     * @return List of String of StorageProviders' identifier
     */
    List<String> getConnectedStorageProviders();

    /***
     * sends data to a specific storage providers
     * @param filename
     * @param data
     * @param storageProvider
     */
    boolean sendDataToServer(String filename, byte[] data, String storageProvider);

    /***
     * sends data to all storage providers
     * @param filename
     * @param data
     * @return list of storage providers the file was sent too
     */
    List<IStorageProvider> sendDataToServer(String filename, byte[] data);

    /***
     * gets the byte-data of a given file from a specific storage provider
     * @param filename
     * @param storageProvider
     * @return byte[] data of file
     */
    byte[] getDataFromServer(String filename, String storageProvider);

    /***
     * get the byte-data of a given file from all connected storage providers
     * @param filename
     * @return returns a map of the identifier and byte[] from every storage providers
     */
    Map<String, byte[]> getDataFromServer(String filename);

    /***
     * delete filename from all storage providers
     * @param filename
     */
    void deleteDataFromServer(String filename);

    /***
     * delete filename from a specific storage provider
     * @param filename
     * @param storageProvider
     * @return byte[] data of file
     */
    void deleteDataFromServer(String filename, String storageProvider);

    /**
     * check to see if storage providers are online
     *
     * @return map with storage provider unique identifier and its status
     */
    Map<String, Boolean> getServerStatus();

}
