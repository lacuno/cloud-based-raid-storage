package storageProvider;

import app.Modules;
import com.box.boxjavalibv2.BoxClient;
import com.box.boxjavalibv2.BoxConfigBuilder;
import com.box.boxjavalibv2.IBoxConfig;
import com.box.boxjavalibv2.authorization.OAuthRefreshListener;
import com.box.boxjavalibv2.dao.*;
import com.box.boxjavalibv2.exceptions.AuthFatalFailureException;
import com.box.boxjavalibv2.exceptions.BoxJSONException;
import com.box.boxjavalibv2.exceptions.BoxServerException;
import com.box.boxjavalibv2.jsonparsing.BoxJSONParser;
import com.box.boxjavalibv2.jsonparsing.BoxResourceHub;
import com.box.boxjavalibv2.resourcemanagers.BoxFoldersManageImpl;
import com.box.restclientv2.exceptions.BoxRestException;
import com.box.restclientv2.requestsbase.BoxFileUploadRequestObject;
import play.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author thiago
 */

public class BoxStorageProvider implements IStorageProvider {

    private final String user_name = "cloudraid2014@gmail.com";

    private List<String> credentials;

    private HashMap<String, String> fileList;

    public BoxStorageProvider() throws StorageProviderException {

        credentials = Modules.getCredentialProperty("box", new ArrayList<String>() {{
            add("box_token"); //key
            add("secret_access_key"); //secret
            add("access_token");//access_token
            add("refresh_token");//refresh_token
        }});

        //boxClient = initializeClient();
    }

    private BoxClient initializeClient() throws StorageProviderException {
        if(this.credentials == null) {
            throw new StorageProviderException("Credential file is missing for box");
        }

        BoxResourceHub hub = new BoxResourceHub();
        BoxJSONParser parser = new BoxJSONParser(hub);
        IBoxConfig config = (new BoxConfigBuilder()).build();
        BoxClient client = new BoxClient(this.credentials.get(0), this.credentials.get(1), hub, parser, config);

        HashMap<String, Object> tokenMap = new HashMap<String, Object>();
        tokenMap.put("access_token", this.credentials.get(2));
        tokenMap.put("refresh_token", this.credentials.get(3));
        BoxOAuthToken token = new BoxOAuthToken(tokenMap);
        client.authenticate(token);

        client.addOAuthRefreshListener(new OAuthRefreshListener()  {
            @Override
            public void onRefresh(IAuthData newAuthData) {

                PrintWriter writer = null;
                try {
                    writer = new PrintWriter("../box/credentials", "UTF-8");
                    writer.println("box_token="+credentials.get(0));
                    writer.println("secret_access_key="+credentials.get(1));
                    writer.println("access_token="+ newAuthData.getAccessToken());
                    writer.println("refresh_token="+ newAuthData.getRefreshToken());
                } catch (FileNotFoundException e) {
                    Logger.error(e.getMessage());
                } catch (UnsupportedEncodingException e) {
                    Logger.error(e.getMessage());
                } finally {
                    writer.close();
                }
            }
        });

        return client;
    }

    @Override
    public boolean isOnline() {
        try {
            initializeClient().getFoldersManager().getFolder("0", null);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    public byte[] getFile(String filepath) throws StorageProviderException {

        if (filepath == null) {
            throw new StorageProviderException("filepath can't be null");
        }

        try {
            BoxClient boxClient = initializeClient();

            if (fileList == null) {
                initializeFileList(boxClient);
            }

            if (fileList.containsKey(filepath)) {
                BoxFile boxFile = boxClient.getFilesManager().getFile(fileList.get(filepath), null);

                InputStream is;

                is = boxClient.getFilesManager().downloadFile(boxFile.getId(), null);
                ByteArrayOutputStream buffer = new ByteArrayOutputStream();

                int nRead;
                byte[] data = new byte[16384];

                while ((nRead = is.read(data, 0, data.length)) != -1) {
                    buffer.write(data, 0, nRead);
                }

                buffer.flush();

                return buffer.toByteArray();

            } else {
                return new byte[0];
                //throw new StorageProviderException("file not found");
            }
        } catch (BoxRestException e) {
            throw new StorageProviderException(e);
        } catch (BoxServerException e) {
            return new byte[0];
            //throw new StorageProviderException(e);
        } catch (AuthFatalFailureException e) {
            throw new StorageProviderException(e);
        } catch (IOException e) {
            throw new StorageProviderException(e);
        }
    }

    @Override
    public void deleteFile(String filepath) throws StorageProviderException {
        if (filepath == null) {
            throw new StorageProviderException("filepath can't be null");
        }

        try {

            BoxClient boxClient = initializeClient();

            if (fileList == null) {
                initializeFileList(boxClient);
            }

            if (fileList.containsKey(filepath)) {
                BoxFile boxFile = boxClient.getFilesManager().getFile(fileList.get(filepath), null);

                boxClient.getFilesManager().deleteFile(boxFile.getId(), null);
                fileList.remove(filepath);
            } else {
                throw new StorageProviderException("file not found");
            }

        } catch (BoxRestException e) {
            throw new StorageProviderException(e);
        } catch (BoxServerException e) {
            //fileList.remove(filepath);
            throw new StorageProviderException(e);
        } catch (AuthFatalFailureException e) {
            throw new StorageProviderException(e);
        }


    }

    @Override
    public void postFile(String filepath, byte[] filedata) throws StorageProviderException {

        if (filepath == null) {
            throw new StorageProviderException("filepath can't be null");
        }
        if (filedata == null) {
            throw new StorageProviderException("filedata can't be null");
        }

        try {

            BoxClient boxClient = initializeClient();
            BoxFolder boxFolder = boxClient.getFoldersManager().getFolder("0", null);
            BoxFile boxFile;

            ByteArrayInputStream inputStream = new ByteArrayInputStream(filedata);

            if (fileList == null) {
                initializeFileList(boxClient);
            }

            if (fileList.containsKey(filepath)) {
                boxFile = null;
                try {
                    boxFile = boxClient.getFilesManager().getFile(fileList.get(filepath), null);
                } catch(BoxServerException e) {

                }
                BoxFileUploadRequestObject requestObj = BoxFileUploadRequestObject.uploadFileRequestObject(boxFolder.getId(), filepath, inputStream);

                if(boxFile == null) {
                    boxClient.getFilesManager().uploadFile(requestObj);
                } else {
                    boxClient.getFilesManager().uploadNewVersion(boxFile.getId(), requestObj);
                }
            } else {
                BoxFileUploadRequestObject requestObj = BoxFileUploadRequestObject.uploadFileRequestObject(boxFolder.getId(), filepath, inputStream);
                boxFile = boxClient.getFilesManager().uploadFile(requestObj);
                fileList.put(filepath, boxFile.getId());
            }


        } catch (BoxRestException e) {
            throw new StorageProviderException(e);
        } catch (BoxJSONException e) {
            throw new StorageProviderException(e);
        } catch (InterruptedException e) {
            throw new StorageProviderException(e);
        } catch (BoxServerException e) {
            throw new StorageProviderException(e);
        } catch (AuthFatalFailureException e) {
            throw new StorageProviderException(e);
        }
    }

    @Override
    public String getUniqueIdentifier() {
        return "Box:" + user_name;
    }

    private void initializeFileList(BoxClient boxClient) throws BoxServerException, AuthFatalFailureException, BoxRestException {
        fileList = new HashMap<String, String>();
        BoxFolder boxFolder = boxClient.getFoldersManager().getFolder("0", null);
        ArrayList<BoxTypedObject> folderEntries = boxFolder.getItemCollection().getEntries();
        int folderSize = folderEntries.size();
        for (BoxTypedObject folderEntry : folderEntries) {
            if (folderEntry instanceof BoxItem) {
                BoxItem bI = (BoxItem) folderEntry;

                fileList.put(bI.getName(), bI.getId());
            }
        }

    }

}
