package storageProvider;

/**
 * @author David
 */
public interface IStorageProvider {

    /**
     * request the specified file from the storage provider.
     *
     * @param filepath
     * @return byte[] file data
     * @throws StorageProviderException
     */
    byte[] getFile(String filepath) throws StorageProviderException;

    /**
     * deletes the specified file from the storage provider. If file isn't available method is going to terminate
     *
     * @param filepath
     * @throws StorageProviderException
     */
    void deleteFile(String filepath) throws StorageProviderException;

    /**
     * creates a new file on the storage providers space and saves the filedata into it
     *
     * @param filepath
     * @throws StorageProviderException
     */
    void postFile(String filepath, byte[] filedata) throws StorageProviderException;

    /**
     * check for online/offline status
     *
     * @return true if online, false otherwise
     */
    boolean isOnline();

    /**
     * getUniqueIdentifier returns a unique identifier for the storage provider. The concation is
     * provider-name:user-credential-name
     *
     * @return String unique identifier
     */
    String getUniqueIdentifier();
}
