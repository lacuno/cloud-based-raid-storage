package storageProvider;

/**
 * @author David.
 */
public class StorageProviderException extends Exception {

    public StorageProviderException(String message) {
        super(message);
    }

	public StorageProviderException(Exception e) {
		super(e);
	}

}
