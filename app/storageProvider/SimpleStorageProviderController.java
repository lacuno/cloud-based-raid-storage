package storageProvider;

import play.Logger;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Francesco.
 */

@Singleton
public class SimpleStorageProviderController implements IStorageProviderController {

    private static ArrayList<IStorageProvider> storage_providers;

    @Inject
    public SimpleStorageProviderController() {
        storage_providers = new ArrayList<IStorageProvider>();

        AmazonStorageProvider asp = new AmazonStorageProvider();
        storage_providers.add(asp);

        try {
            BoxStorageProvider box = new BoxStorageProvider();
            storage_providers.add(box);
        } catch (StorageProviderException e) {
            Logger.error("I couldn't instantiate box storage provider");
        }

        try {
            DropboxStorageProvider dsp = new DropboxStorageProvider();
            storage_providers.add(dsp);
        } catch (StorageProviderException ex) {
            Logger.error("I couldn't instantiate dropbox storage provider");
        }
    }

    @Override
    public List<String> getConnectedStorageProviders() {
        ArrayList<String> result = new ArrayList<String>();
        for (IStorageProvider sp: storage_providers) {
            result.add(sp.getUniqueIdentifier());
        }
        return result;
     }

    @Override
    public boolean sendDataToServer(String filename, byte[] data, String storageProvider){
        if (filename == null || data == null || storageProvider == null) return false;
        for (IStorageProvider sp: storage_providers) {
            if (sp.getUniqueIdentifier().equals(storageProvider)) {
                try {
                    sp.postFile(filename, data);
                    return true;
                } catch (StorageProviderException ex) {
                    Logger.error("I could't send data to " + storageProvider);
                    return false;
                }
            }
        }
        return false;
    }

    @Override
    public List<IStorageProvider> sendDataToServer(String filename, byte[] data) {
        if (filename == null || data == null) return null;
        List<IStorageProvider> storageProviders = new ArrayList();
        for (IStorageProvider sp: storage_providers) {
            try {
                sp.postFile(filename, data);
                storageProviders.add(sp);
            } catch (StorageProviderException ex) {
                Logger.error(ex.getMessage());
                Logger.error("I could't send data to " + sp.getUniqueIdentifier());
            }
        }
        return storageProviders;
    }

    @Override
    public byte[] getDataFromServer(String filename, String storageProvider) {
        if (filename == null || storageProvider == null) return null;
        for (IStorageProvider sp: storage_providers) {
            if (sp.getUniqueIdentifier().equals(storageProvider)) {
                try {
                    return sp.getFile(filename);
                } catch (StorageProviderException ex) {
                    Logger.error("I could't retrieve data from " + storageProvider);
                }
            }
        }
        return null;
    }

    @Override
    public Map<String, byte[]> getDataFromServer(String filename) {
        if (filename == null) return null;
        Map<String, byte[]> result = new HashMap<String, byte[]>();

        for (IStorageProvider sp: storage_providers) {
            try {
                result.put(sp.getUniqueIdentifier(), sp.getFile(filename));
            } catch (StorageProviderException ex) {
                result.put(sp.getUniqueIdentifier(), null);
                Logger.error("I could't retrieve data from " + sp.getUniqueIdentifier());
            }
        }

        return result;
    }

    @Override
    public void deleteDataFromServer(String filename) {
        if (filename == null) return;
        for (IStorageProvider sp: storage_providers) {
            try {
                sp.deleteFile(filename);
            } catch (StorageProviderException ex) {
                Logger.error("I could't delete data from " + sp.getUniqueIdentifier());
            }
        }
    }

    @Override
    public void deleteDataFromServer(String filename, String storageProvider) {
        if (filename == null) return;
        for (IStorageProvider sp: storage_providers) {
            if (sp.getUniqueIdentifier().equals(storageProvider)) {
                try {
                    sp.deleteFile(filename);
                } catch (StorageProviderException ex) {
                    Logger.error("I could't delete data from " + sp.getUniqueIdentifier());
                }
            }
        }
    }

    @Override
    public Map<String, Boolean> getServerStatus() {
        Map<String, Boolean> map = new HashMap<>();
        for (IStorageProvider sp : storage_providers) {
            map.put(sp.getUniqueIdentifier(), sp.isOnline());
        }
        return map;
    }
}
