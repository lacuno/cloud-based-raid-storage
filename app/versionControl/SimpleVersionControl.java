package versionControl;

import data.DataException;
import data.IFileController;
import data.IFilePartsController;
import data.model.File;
import data.model.FilePart;
import raid.RaidLevel;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

/**
 * @author David.
 */

@Singleton
public class SimpleVersionControl implements IVersionControl {

    private IFileController _fileController;
    private IFilePartsController _filePartsController;

    @Inject
    public SimpleVersionControl(IFileController fileController, IFilePartsController filePartsController) {
        this._fileController = fileController;
        this._filePartsController = filePartsController;
    }

    @Override
    public boolean fileExists(String filename) throws VersionControlException {
        try {
            return _fileController.getFiles(filename).size() > 0;
        } catch (DataException e) {
            throw new VersionControlException(e);
        }
    }

    @Override
    public boolean fileExists(String filename, int version) throws VersionControlException {
        try {
            return _fileController.getFile(filename, version) != null;
        } catch (DataException e) {
            throw new VersionControlException(e);
        }
    }

    @Override
    public int getLatestFileVersion(String filename) throws VersionControlException {
        return _fileController.getFileVersion(filename);
    }

    @Override
    public List<File> getFileVersions(String filename) throws VersionControlException {
        try {
            List<File> files = _fileController.getFiles(filename);
            for(File file : files) {
                file.setFileParts(_filePartsController.getFileparts(file.getName(), file.getVersion()));
            }

            return files;
        } catch (DataException e) {
            throw new VersionControlException(e);
        }
    }

    @Override
    public List<File> getFiles() throws VersionControlException {
        try {
            List<File> files = _fileController.getFiles();
            for(File file : files) {
                file.setFileParts(_filePartsController.getFileparts(file.getName(), file.getVersion()));
            }

            return files;
        } catch (DataException e) {
            throw new VersionControlException(e);
        }
    }

    @Override
    public File getFileInVersion(String filename, int version) throws VersionControlException {
        try {
            File file = _fileController.getFile(filename, version);
            List<FilePart> fileparts = _filePartsController.getFileparts(filename, version);
            file.setFileParts(fileparts);
            return file;
        } catch (DataException e) {
            throw new VersionControlException(e);
        }
    }

    @Override
    public File getFileInLatestVersion(String filename) throws VersionControlException {
        int highestFileVersion = _fileController.getFileVersion(filename);
        if (highestFileVersion == 0) {
            throw new VersionControlException("No file exists with that filename");
        }

        try {
            File file =_fileController.getFile(filename, highestFileVersion);
            file.setFileParts(_filePartsController.getFileparts(file.getName(), file.getVersion()));
            return file;
        } catch (DataException e) {
            throw new VersionControlException(e);
        }
    }

    @Override
    public File createNewFileVersion(String filename, RaidLevel raidLevel, byte[] data) throws VersionControlException {
        int highestFileVersion = _fileController.getFileVersion(filename);

        File f = new File(filename, highestFileVersion + 1, raidLevel, Calendar.getInstance(TimeZone.getTimeZone("UTC")), false);
        f.setChecksum(data);
        return f;
    }

    @Override
    public void saveFile(File file, List<FilePart> fileParts) throws VersionControlException {
        try {
            _fileController.putFile(file);
            for (FilePart filePart : fileParts) {
                _filePartsController.putFilePart(file.getName(), file.getVersion(), filePart.getStorageProviderId(), filePart.getChecksum(), filePart.isParity(), filePart.getOrder());
            }
        } catch (DataException e) {
            e.printStackTrace();
            throw new VersionControlException(e);
        }
    }

    @Override
    public void markFile(String filename, int version) throws VersionControlException {
        try {
            _fileController.markFile(filename, version);
        } catch (DataException e) {
            e.printStackTrace();
            throw new VersionControlException(e);
        }
    }

    @Override
    public void deleteFile(String filename) throws VersionControlException {
        try {
            _filePartsController.deleteFileParts(filename);
            _fileController.deleteFile(filename);
        } catch (DataException e) {
            throw new VersionControlException(e);
        }
    }

    @Override
    public void deleteFile(String filename, int version) throws VersionControlException {
        try {
            _filePartsController.deleteFilePart(filename, version);
            _fileController.deleteFile(filename, version);
        } catch (DataException e) {
            throw new VersionControlException(e);
        }
    }

}
