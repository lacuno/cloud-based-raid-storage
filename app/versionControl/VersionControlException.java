package versionControl;

/**
 * @author David.
 */
public class VersionControlException extends Exception {

    public VersionControlException(String message) {
        super(message);
    }
    public VersionControlException(Exception message) {
        super(message);
    }

}
