package versionControl;

import data.model.File;
import data.model.FilePart;
import raid.RaidLevel;

import java.util.List;

/**
 * @author David.
 */
public interface IVersionControl {

    /**
     * fileExists determines if there is a valid version for given file
     *
     * @param filename
     * @return bool if file exists in version control
     * @throws VersionControlException
     */
    boolean fileExists(String filename) throws VersionControlException;

    /**
     * fileExists determines if there is a file in the specified version
     *
     * @param filename
     * @param version
     * @return bool if the file in the version exists
     * @throws VersionControlException
     */
    boolean fileExists(String filename, int version) throws VersionControlException;

    /**
     * getLastestFileVersion returns the last valid version for a given file
     *
     * @param filename
     * @return int latest file version number
     * @throws VersionControlException
     */
    int getLatestFileVersion(String filename) throws VersionControlException;

    /**
     * getFileVersions returns a list of file versions for a given filename
     *
     * @param filename
     * @return list of all file versions
     * @throws VersionControlException
     */
    List<File> getFileVersions(String filename) throws VersionControlException;

    /**
     * getFiles returns all files and all versions of a file
     *
     * @return list of all files and versions
     * @throws VersionControlException
     */
    List<data.model.File> getFiles() throws VersionControlException;

    /**
     * getFileInVersion returns a file in a specific version
     *
     * @param filename
     * @param version
     * @return specific file version
     * @throws VersionControlException
     */
    data.model.File getFileInVersion(String filename, int version) throws VersionControlException;

    /**
     * getFileInLatestVersion returns a file in the lastest version
     *
     * @param filename
     * @return file in latest version
     * @throws VersionControlException
     */
    data.model.File getFileInLatestVersion(String filename) throws VersionControlException;

    /**
     * createNewFileVersion returns a new file model with in the newest version
     *
     * @param filename
     * @return file in new (latest) version
     * @throws VersionControlException
     */
    data.model.File createNewFileVersion(String filename, RaidLevel raidLevel, byte[] data) throws VersionControlException;

    /**
     * saveFile saves file version to version control store
     *  @param file
     *  @param fileparts
     * @throws VersionControlException
     */
    void saveFile(data.model.File file, List<FilePart> fileparts) throws VersionControlException;


    /**
     * markFile mark file
     * @param filename
     * @param version
     * @throws VersionControlException
     */
    void markFile(String filename, int version) throws VersionControlException;


    /**
     * deleteFile delete file via filename
     * @param filename
     * @throws VersionControlException
     */
    public void deleteFile(String filename) throws VersionControlException;

    /**
     * deleteFile delete file via version
     * @param filename
     * @param version
     * @throws VersionControlException
     */
    public void deleteFile(String filename, int version) throws VersionControlException;
}
