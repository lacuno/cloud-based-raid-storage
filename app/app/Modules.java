package app;

import play.Logger;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

/**
 * @author David
 */
public class Modules {

    private static HashMap<String, String> credentialFilePaths = new HashMap<String, String>();

    /**
     * getCredentialProperty get Credential property
     * @param storageProvider
     * @param propertyNames
     * @return list of file storage provider credentials property
     */
    public static List<String> getCredentialProperty(String storageProvider, List<String> propertyNames) {
        if (credentialFilePaths.containsKey(storageProvider)) {
            InputStream input = null;
            try {
                input = new FileInputStream(credentialFilePaths.get(storageProvider));
            } catch (FileNotFoundException e) {
                Logger.error(e.getMessage());
                return null;
            }
            Properties prop = new Properties();

            try {
                prop.load(input);
            } catch (IOException e) {
                Logger.error(e.getMessage());
                return null;
            }

            List<String> retProperties = new ArrayList<String>();
            for (String propertyName : propertyNames) {
                retProperties.add(prop.getProperty(propertyName));
            }

            return retProperties;
        }

        return null;
    }

    /**
     * addCredentialFilePath add credential file path
     * @param storageprovider
     * @param filepath
     */
    public static void addCredentialFilePath(String storageprovider, String filepath) {
        credentialFilePaths.put(storageprovider, filepath);
    }


}
