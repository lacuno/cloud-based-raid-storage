package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import data.model.FilePart;
import org.apache.commons.io.IOUtils;
import play.Logger;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import raid.IRaidController;
import raid.IRaidLevel;
import raid.RaidException;
import raid.RaidLevel;
import versionControl.IVersionControl;
import versionControl.VersionControlException;

import javax.inject.Inject;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author David.
 */
public class File extends Controller {

    private static IVersionControl _versionControl;

    private static IRaidController _raidController;

    @Inject
    public File(IVersionControl versionControl, IRaidController raidController) {
        this._versionControl = versionControl;
        this._raidController = raidController;

        /*
        ObjectMapper mapper = new ObjectMapper();
        SimpleDateFormat outputFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss Z");
        mapper.setDateFormat(outputFormat);
        Json.setObjectMapper(mapper);
        */
    }

    /**
     * getServerStatus get server status
     * @return json server status
     */
    public Result getServerStatus() {
        Map<String, Boolean> map = _raidController.getServerStatus();
        JsonNode json = Json.toJson(map);
        return ok(json);
    }


    /**
     * listFiles list of files
     * @return json list of files
     */
    public Result listFiles() {
        if (_versionControl != null) {
            try {
                List<data.model.File> fileList = _versionControl.getFiles();
                JsonNode json = Json.toJson(fileList);

                return ok(json);
            } catch (VersionControlException vce) {
                // log error somewhere log4j
                //de.fillInStackTrace();
                return badRequest(vce.getMessage());
            }
        }

        return badRequest("versionControl couldn't be accessed.");

    }

    /**
     * listFileVersions list of file versions
     * @return json list file versions
     */
    public Result listFileVersions(String filename) {
        if (_versionControl != null) {
            try {
                List<data.model.File> fileList = _versionControl.getFileVersions(filename);
                JsonNode json = Json.toJson(fileList);

                return ok(json);
            } catch (VersionControlException vce) {
                return badRequest(vce.getMessage());
            }
        }

        return badRequest("versionControl couldn't be accessed.");
    }

    /**
     * getFile get file by filename and version
     * @param filename
     * @param version
     * @return json get file
     */
    public Result getFile(String filename, int version) {
        if (_versionControl != null && _raidController != null) {
            try {
                data.model.File file = _versionControl.getFileInVersion(filename, version);
                IRaidLevel raidLevel = null;
                if (file == null) {
                    return badRequest("File doesn't exist");
                }
                switch (file.getRaidLevel()) {
                    case Level1:
                        raidLevel = _raidController.getRaid1Instance();
                        break;

                    case Level5:
                        raidLevel = _raidController.getRaid5Instance();
                        break;
                }

                if (raidLevel != null) {
                    byte[] data = null;
                    try {
                        data = raidLevel.getFile(file.getFullFilename(), file.getChecksum(), file.getFileParts());
                        if (data != null) {
                            response().setHeader("Content-disposition", "attachment; filename=" + file.getName());
                            return ok(data);
                        } else {
                            return badRequest("raid could not find file");
                        }
                    } catch (RaidException e) {

                         if (e.ismarkerd()){
                             _versionControl.markFile(filename,version);
                         }

                        return badRequest(e.getMessage());

                    }

                } else {
                    return badRequest("illegal raid level chosen");
                }
            } catch (VersionControlException vce) {
                return badRequest(vce.getMessage());
            }
        }

        return badRequest("versionControl or raidController couldn't be accessed.");
    }

    /**
     * deleteFileVersions delete file by filename
     * @param filename
     * @return json delete file
     */
    public Result deleteFileVersions(String filename) {
        if (_versionControl != null && _raidController != null) {
            try {
                List<data.model.File> files = _versionControl.getFileVersions(filename);

                if (files.size() == 0) {
                    return badRequest("File doesn't exist");
                }

                for(data.model.File file : files) {
                    IRaidLevel raidLevel = null;
                    switch (file.getRaidLevel()) {
                        case Level1:
                            raidLevel = _raidController.getRaid1Instance();
                            break;

                        case Level5:
                            raidLevel = _raidController.getRaid5Instance();
                            break;
                    }

                    if (raidLevel != null) {
                        try {
                            raidLevel.deleteFile(file.getFullFilename());
                        } catch (RaidException e) {
                            Logger.error(e.getMessage());
                        }
                    } else {
                        Logger.error("illegal raid level chosen");
                    }
                }

                _versionControl.deleteFile(files.get(0).getName());
            } catch (VersionControlException vce) {
                return badRequest(vce.getMessage());
            }

            return ok("File deleted successfully");
        }

        return badRequest("versionControl or raidController couldn't be accessed.");
    }


    /**
     * deleteFile delete file by filename   and version
     * @param filename
     * @return json delete file
     */
    public Result deleteFile(String filename, int version) {
        if (_versionControl != null && _raidController != null) {
            try {
                data.model.File file = _versionControl.getFileInVersion(filename, version);
                IRaidLevel raidLevel = null;
                if (file == null) {
                    return badRequest("File doesnt exist");
                }
                switch (file.getRaidLevel()) {
                    case Level1:
                        raidLevel = _raidController.getRaid1Instance();
                        break;

                    case Level5:
                        raidLevel = _raidController.getRaid5Instance();
                        break;
                }

                if (raidLevel != null) {
                    try {
                        raidLevel.deleteFile(file.getFullFilename());
                        _versionControl.deleteFile(filename,version);
                        return ok("File deleted successfully");
                    } catch (RaidException e) {
                        return badRequest(e.getMessage());
                    }

                } else {
                    return badRequest("illegal raid level chosen");
                }
            } catch (VersionControlException vce) {
                return badRequest(vce.getMessage());
            }
        }

        return badRequest("versionControl or raidController couldn't be accessed.");
    }

    /**
     * createFile creeate file by filename with a raid lavel
     * @param filename
     * @param raidLevel
     * @return json create file
     */
    public Result createFile(String filename, String raidLevel) {
        if (_versionControl != null && _raidController != null) {
            byte[] fileData = null;
            Http.MultipartFormData body = request().body().asMultipartFormData();
            Http.MultipartFormData.FilePart formFile = body.getFile("file");

            if (formFile != null) {
                try {
                    fileData = IOUtils.toByteArray(new FileInputStream(formFile.getFile()));
                } catch (IOException e) {
                    Logger.error(e.getMessage());
                }
                if (fileData == null || fileData.length == 0) {
                    return badRequest("No file data found in request");
                }
            } else {
                return badRequest("No file data found in request");
            }


            RaidLevel raidLvl;
            try {
                raidLvl = RaidLevel.valueOf(raidLevel);
            } catch (Exception e) {
                return badRequest("Given raid level was not a valid Level");
            }

            try {
                data.model.File file = _versionControl.createNewFileVersion(filename, raidLvl, fileData);
                IRaidLevel iRaidLevel = null;
                switch (file.getRaidLevel()) {
                    case Level1:
                        iRaidLevel = _raidController.getRaid1Instance();
                        break;

                    case Level5:
                        iRaidLevel = _raidController.getRaid5Instance();
                        break;
                }

                if (raidLevel != null) {
                    try {
                        List<FilePart> filepartsDistributed = iRaidLevel.postFile(file.getFullFilename(), fileData);
                        _versionControl.saveFile(file, filepartsDistributed);
                        return ok("File uploaded successfully");
                    } catch (RaidException e) {
                        return badRequest(e.getMessage());
                    }

                } else {
                    return badRequest("illegal raid level chosen");
                }
            } catch (VersionControlException vce) {
                return badRequest(vce.getMessage());
            }
        }

        return badRequest("versionControl or raidController couldn't be accessed.");
    }

}
