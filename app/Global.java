import app.Modules;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import data.FileController;
import data.FilePartsController;
import data.IFileController;
import data.IFilePartsController;
import play.Application;
import play.GlobalSettings;
import play.Logger;
import raid.IRaidController;
import raid.SimpleRaidController;
import storageProvider.IStorageProviderController;
import storageProvider.QueuedStorageProviderController;
import versionControl.IVersionControl;
import versionControl.SimpleVersionControl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;

/**
 * @author David.
 */

public class Global extends GlobalSettings {
    private Injector injector = Guice.createInjector(new AbstractModule() {

        @Override
        protected void configure() {
            bind(IFileController.class).to(FileController.class);
            bind(IFilePartsController.class).to(FilePartsController.class);
            bind(IVersionControl.class).to(SimpleVersionControl.class);
            bind(IRaidController.class).to(SimpleRaidController.class);
            bind(IStorageProviderController.class).to(QueuedStorageProviderController.class);
        }
    });

    /**
     * Get controller instances
     *
     * @param  aClass
     */
    @Override
    public <A> A getControllerInstance(Class<A> aClass) {
        return injector.getInstance(aClass);
    }

    /**
     * Load all storage credentials files when program starts.
     *
     * @param  app
     */
    @Override
    public void onStart(Application app) {
        Logger.info("Application has started");
        InputStream input = null;
        try {
            input = new FileInputStream("conf/paths.properties");
        } catch (FileNotFoundException e) {
            Logger.error(e.getMessage());
        }
        Properties prop = new Properties();

        try {
            prop.load(input);
        } catch (IOException e) {
            Logger.error(e.getMessage());
        }

        Enumeration e = prop.propertyNames();

        while (e.hasMoreElements()) {
            String key = (String) e.nextElement();
            Modules.addCredentialFilePath(key, prop.getProperty(key));
        }
    }
}
