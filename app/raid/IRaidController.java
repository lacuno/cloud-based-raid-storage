package raid;

import java.util.Map;

/**
 * @author David.
 */
public interface IRaidController {

    /**
     * getRaid1Instance returns an instance of RaidLevel1
     *
     * @return raid1 instance
     */
    IRaidLevel getRaid1Instance();

    /**
     * getRaid5Instance returns an instance of RaidLevel5
     *
     * @return raid5 instance
     */
    IRaidLevel getRaid5Instance();

    /**
     * check every storage provider to see if it is online
     *
     * @return map containing unique storage provider identifiers and their status
     */
    Map<String, Boolean> getServerStatus();
}
