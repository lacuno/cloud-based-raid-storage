package raid;

import data.model.FilePart;

import java.util.List;

/**
 *  @author David
 */
public interface IRaidLevel {

    /**
     * getFile request given filepath from every connetected storage provider, computes hashes of the received files
     * and will repair (delete and reupload) corrupted files - finally returns file data byte[]
     * RAID5 logic includes setting together file parts and recreating file
     *
     * @param filepath
     * @param hash
     * @return byte[] file data
     * @throws RaidException
     */
    byte[] getFile(String filepath, byte[] hash, List<FilePart> fileParts) throws RaidException;

    /**
     * deleteFile deletes the specified file on all storage providers.
     *
     * @param filepath
     * @throws RaidException
     */
    void deleteFile(String filepath) throws RaidException;

    /*
        raid layer shouldn't even know of any "version" stuff
     */
    //void deleteFile (String filepath, int version) throws RaidException;

    /**
     * postFile will use raid logic to distribute file on all connected storage providers
     *
     * @param filepath
     * @param filedata
     * @throws RaidException
     */
    List<FilePart> postFile(String filepath, byte[] filedata) throws RaidException;

}
