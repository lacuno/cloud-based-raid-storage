package raid;

import data.model.File;
import data.model.FilePart;
import storageProvider.IStorageProviderController;

import java.lang.System;
import java.util.*;
import java.util.ArrayList;

/**
 * @author Christoph.
 */
public class RaidLevel5 implements IRaidLevel {
    private IStorageProviderController storageProviderController;

    public RaidLevel5(IStorageProviderController _storageProviderController) {
        this.storageProviderController = _storageProviderController;
    }

    @Override
    public byte[] getFile(String filepath, byte[] hash, List<FilePart> fileParts) throws RaidException {
        if (filepath == null || hash == null) return null;
        Map<String, byte[]> retrieved = storageProviderController.getDataFromServer(filepath);

        // Datastuctures for determining the wrong server and reconstruction
        List<FilePart> correctFileParts = new ArrayList<FilePart>();
        List<byte[]> correctData = new ArrayList<byte[]>();
        FilePart incorrectFilePart = null;


        byte[][] dataFromServers = new byte[3][];

        // Get parityserver
        int parityserver = -1;
        for(FilePart filePart : fileParts) {
            if(filePart.isParity()) {
                parityserver = filePart.getOrder();
            }
        }
        System.out.println("ParityServer: " + parityserver);

        boolean reparable = false;
        // Check if all gathered parts are correct
        int counter = 0;
        for(FilePart filePart : fileParts) {
            dataFromServers[counter] = retrieved.get(filePart.getStorageProviderId());
            if(dataFromServers[counter] == null) {
                reparable = true;
            }

            byte[] checksum = File.generateHash(dataFromServers[counter]);
            if(Arrays.equals(checksum, filePart.getChecksum())) {
                correctFileParts.add(filePart);
                correctData.add(dataFromServers[counter]);
            } else {
                incorrectFilePart = filePart;
            }

            if(filePart.isParity()) {
                parityserver = counter;
            }
            counter++;
        }

        if(correctFileParts.size() < 2) {
            if(!reparable) {
                throw new RaidException("RAID5 is not reconstructable: less than two servers contain correct file-data", true);
            } else {
                throw new RaidException("RAID5 is not reconstructable: less than two servers contain correct file-data");
            }
        } else if(correctFileParts.size() == 2) {
            System.out.println("Correction needed");
            // Do correction
            byte[][] correctDataArray = new byte[2][];

            for(int i = 0; i < 2; i++) {
                correctDataArray[i] = correctData.get(i);
            }
            byte[] correctedData = raid5Restore(correctDataArray);
            dataFromServers[incorrectFilePart.getOrder()] = correctedData;
            if(!reparable) {
                storageProviderController.sendDataToServer(filepath, correctedData, incorrectFilePart.getStorageProviderId());
            }

        }

        byte[] filedata = raid5Merging(dataFromServers, parityserver);
        if(!Arrays.equals(File.generateHash(filedata), hash)) {
            throw new RaidException("File is corrupt (But fileservers seem ok)");
        }

        return filedata;
    }

    @Override
    public void deleteFile(String filepath) throws RaidException {
        if (filepath == null) return;
        storageProviderController.deleteDataFromServer(filepath);
    }

    @Override
    public List<FilePart> postFile(String filepath, byte[] filedata) throws RaidException {
        if (filepath == null || filedata == null) return null;

        int parityserver = (int)(Math.random() * 3);
        List<String> storageProviders = storageProviderController.getConnectedStorageProviders();
        byte[][] splittedFileData = raid5Splitting(filedata, 3, parityserver);
        List<FilePart> filepartsDistributed = new ArrayList<FilePart>();

        // Send splitted data to servers
        List<String> succeededStorageProviders = new ArrayList<String>();
        String failed_server = "";
        for(int i = 0; i < storageProviders.size(); i++) {
            System.out.println("Sending data to " + storageProviders.get(i) );
            boolean success = storageProviderController.sendDataToServer(filepath, splittedFileData[i], storageProviders.get(i));
            if(success) {
                succeededStorageProviders.add(storageProviders.get(i));
                byte[] checksum = File.generateHash(splittedFileData[i]);
                FilePart f = new FilePart(filepath, storageProviders.get(i), checksum, i == parityserver, i);
                filepartsDistributed.add(f);
            } else {
                failed_server = storageProviders.get(i);
                break;
            }

            if(succeededStorageProviders.size() == 3) {
                break;
            }
        }

        // Recovery - Files were not distributed to all servers!
        if(succeededStorageProviders.size() != 3) {
            for(String server : succeededStorageProviders) {
                storageProviderController.deleteDataFromServer(filepath, server);
            }
            throw new RaidException("It was not possible to distribute the file to "+failed_server);
        }

        return filepartsDistributed;
    }

    /**
     * Restores the missing part of data by XOR-ing the missing part
     * @param correctdata array holding correct data from servers
     * @return corrected data-part
     */
    private byte[] raid5Restore(byte[][] correctdata) {
        byte[] restoredData = new byte[correctdata[0].length];

        for(int i = 0; i < restoredData.length; i++) {
            byte correctedByte = 0;
            for (byte[] correctpart : correctdata) {
                correctedByte ^= correctpart[i];
            }
            restoredData[i] = correctedByte;
        }

        return restoredData;
    }

    /**
     * Merges a given two dimensional byte array in a RAID5 fashion. Parity is dropped
     * @param filedata
     * @return
     */
    private byte[] raid5Merging(byte[][] filedata, int parityServer) {
        // Offset corrects the length if it was not possible to split the file into evenly big parts
        int offset = 0;
        int lastserver = -1;
        for(int i = filedata.length - 2; i < filedata.length; i++) {
            if(i == parityServer) continue;
            if(filedata[i][filedata[i].length-1] == 0) {
                offset = -1;
            } else {
                offset = 0;
            }
            lastserver = i;
        }
        byte[] mergedData = new byte[(filedata.length -1) * filedata[0].length + offset];

        // Merging
        for(int server = 0, counter = 0; server < filedata.length; server++) {
            if(server == parityServer) continue;

            if(server == lastserver) {
                System.arraycopy(filedata[server], 0, mergedData, counter * filedata[server].length, filedata[server].length + offset);
            } else {
                System.arraycopy(filedata[server], 0, mergedData, counter * filedata[server].length, filedata[server].length);
            }
            counter++;
        }

        return mergedData;
    }

    /**
     * Splits a given file into numberOfServers parts in a RAID5 fashion and calulates the parities.
     * @param filedata
     * @param numberOfServers
     * @return
     */
    private byte[][] raid5Splitting(byte[] filedata, int numberOfServers, int parityServer) {
        int dynamicStripeSize = (int)Math.ceil((double)filedata.length / (numberOfServers - 1));
        byte[][] splittedFile = new byte[numberOfServers][dynamicStripeSize];

        // Split data
        for(int server = 0, counter = 0; server < numberOfServers; server++) {
            if(server == parityServer) continue;

            System.arraycopy(filedata, dynamicStripeSize * counter, splittedFile[server], 0, Math.min(dynamicStripeSize, filedata.length - (server-1) * dynamicStripeSize));
            counter++;
        }

        // Calculate paritiy
        for(int i = 0; i < dynamicStripeSize; i++) {
            byte parity = 0;
            for (int server = 0; server < numberOfServers; server++) {
                if (server == parityServer) continue;

                parity ^= splittedFile[server][i];
            }
            splittedFile[parityServer][i] = parity;
        }

        return splittedFile;
    }
}
