package raid;

import storageProvider.IStorageProviderController;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Map;

/**
 * @author Francesco.
 */

@Singleton
public class SimpleRaidController implements IRaidController {

    private IStorageProviderController _storageProviderController;

    @Inject
    public SimpleRaidController(IStorageProviderController storageProviderController) {
        this._storageProviderController = storageProviderController;
    }

    @Override
    public IRaidLevel getRaid1Instance() {
        return new RaidLevel1(_storageProviderController);
    }

    @Override
    public IRaidLevel getRaid5Instance() {
        return new RaidLevel5(_storageProviderController);
    }

    @Override
    public Map<String, Boolean> getServerStatus() {
        return _storageProviderController.getServerStatus();
    }
}
