package raid;

import data.model.File;
import data.model.FilePart;
import storageProvider.IStorageProvider;
import storageProvider.IStorageProviderController;
import storageProvider.StorageProviderException;

import java.util.*;
import java.util.Map.Entry;

/**
 * @author Francesco.
 */
public class RaidLevel1 implements IRaidLevel {

    private IStorageProviderController storageProviderController;

    public RaidLevel1(IStorageProviderController _storageProviderController) {
        this.storageProviderController = _storageProviderController;
    }

    @Override
    public byte[] getFile(String filepath, byte[] hash, List<FilePart> fileParts) throws RaidException {
        if (filepath == null || hash == null) return null;
        Map<String, byte[]> retrieved = storageProviderController.getDataFromServer(filepath);

        // find if at least one file match with the hash
        // otherwise return RaidException

        String storage_provider_rielable = null;
        HashSet<String> storage_that_dont_match = new HashSet<String>();
        int wrongServers = 0;

        for (Entry<String, byte[]> file : retrieved.entrySet()) {
            boolean relevant = false;
            for(FilePart filePart : fileParts) {
                if(filePart.getStorageProviderId().equals(file.getKey())) {
                    relevant = true;
                }
            }
            if(!relevant) continue;

            if (Arrays.equals(File.generateHash(file.getValue()), hash)) {
                if (storage_provider_rielable == null) {
                    storage_provider_rielable = file.getKey();
                }
            } else {
                if(!(file.getValue() == null)) {
                    storage_that_dont_match.add(file.getKey());
                }
                wrongServers++;
            }
        }

        System.out.println("false: " + storage_that_dont_match.size());
        System.out.println("Retrieved: " + retrieved.size());
        System.out.println("fileparts: " + fileParts.size());
        if (fileParts.size() == storage_that_dont_match.size()) {
            throw new RaidException(" Irreparabel file on the Raid1 ",true);
        }

        if (fileParts.size() == wrongServers) {
            throw new RaidException("No storage match with the hash");
        }

        for (String id : storage_that_dont_match) {
            System.out.println("Delete");
            storageProviderController.deleteDataFromServer(filepath, id);
            storageProviderController.sendDataToServer(filepath, retrieved.get(storage_provider_rielable), id);
        }

        return retrieved.get(storage_provider_rielable);
    }

    @Override
    public void deleteFile(String filepath) throws RaidException {
        if (filepath == null) return;
        storageProviderController.deleteDataFromServer(filepath);
    }

    @Override
    public List<FilePart> postFile(String filepath, byte[] filedata) throws RaidException {
        if (filepath == null || filedata == null) return null;
        List<IStorageProvider> numberOfServersOnline = storageProviderController.sendDataToServer(filepath, filedata);
        if (numberOfServersOnline.size() < 2) {
            if (numberOfServersOnline.size() == 1) {
                try {
                    numberOfServersOnline.get(0).deleteFile(filepath);
                } catch (StorageProviderException e) {}

                throw new RaidException("Only "+numberOfServersOnline.get(0).getUniqueIdentifier()+" is available");
            }
            throw new RaidException("No servers are available");
        }

        List<FilePart> filepartsDistributed = new ArrayList<FilePart>();

        byte[] hash = File.generateHash(filedata);
        for(IStorageProvider storageProvider : numberOfServersOnline) {
            FilePart filePart = new FilePart(filepath, storageProvider.getUniqueIdentifier(), hash, false, -1);
            filepartsDistributed.add(filePart);
        }

        return filepartsDistributed;
    }

}
