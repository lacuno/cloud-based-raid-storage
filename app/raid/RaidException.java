package raid;

/**
 * @author  David .
 */
public class RaidException extends Exception {

     private boolean _markedFile;

    /**
     * RaidException is a exception class for every exception beeing thrown in IRaidLevel or childs of it.
     *
     * @param message
     */
    public RaidException(String message) {
        super(message) ;
        _markedFile = false;
    }

    public boolean ismarkerd() {
        return _markedFile;
    }

    public RaidException(String message, boolean marked ) {
        super(message);
        this._markedFile = marked;
    }

}
