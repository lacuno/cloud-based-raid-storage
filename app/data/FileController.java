package data;

import data.model.File;
import play.db.DB;
import raid.RaidLevel;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * @author Christoph
 */

@Singleton
public class FileController implements IFileController {
    private Connection connection;
    private PreparedStatement getFileStatement;
    private PreparedStatement updateFileStatement;
    private PreparedStatement getFilesStatement;
    private PreparedStatement getAllFilesStatement;
    private PreparedStatement putFileStatementRaid1;
    private PreparedStatement putFileStatementRaid5;
    private PreparedStatement getFilesLatestVersion;
    private PreparedStatement deleteFile;
    private PreparedStatement deleteFiles;

    @Inject
    public FileController() throws DataException {
        connection = DB.getConnection();
        try {
            getFileStatement = connection.prepareStatement("SELECT * FROM files WHERE name = ? AND version = ?");
            updateFileStatement = connection.prepareStatement("UPDATE files SET marked = true WHERE name = ? AND version = ?");
            getFilesStatement = connection.prepareStatement("SELECT * FROM files WHERE name = ?");
            getAllFilesStatement = connection.prepareStatement("SELECT * FROM files INNER JOIN (SELECT name, max(version) as version FROM files GROUP BY name) ss ON (files.name = ss.name AND files.version = ss.version)");
            putFileStatementRaid1 = connection.prepareStatement("INSERT INTO files " +
                    "(name, version, raidlevel, creation_date, filehash) VALUES" +
                    "(?,?,?,?,?);");
            putFileStatementRaid5 = connection.prepareStatement("INSERT INTO files " +
                    "(name, version, raidlevel, creation_date, filehash) VALUES" +
                    "(?,?,?,?,?);");
            this.getFilesLatestVersion = connection.prepareStatement("SELECT max(version) FROM files WHERE name = ?");
            this.deleteFile = connection.prepareStatement("DELETE FROM files WHERE name = ? AND version = ?");
            deleteFiles = connection.prepareStatement("DELETE FROM files WHERE name = ?");
        } catch (SQLException e) {
            throw new DataException(e);
        }
    }

    @Override
    public File getFile(String filename, int version) throws DataException {
        try {
            // Prepare Statement
            getFileStatement.setString(1, filename);
            getFileStatement.setInt(2, version);

            // Query result
            ResultSet rs = getFileStatement.executeQuery();
            if (rs.next()) {
                int raidlevel = rs.getInt("raidlevel");

                Calendar cal = Calendar.getInstance();
                Date creation_date = rs.getTimestamp("creation_date");
                cal.setTime(creation_date);
                byte[] filehash = rs.getBytes("filehash");
                boolean marked = rs.getBoolean("marked");
                if (raidlevel == 1) {
                    return new File(filename, version, RaidLevel.Level1, cal, filehash, marked);
                } else {
                    return new File(filename, version, RaidLevel.Level5, cal, filehash, marked);
                }
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new DataException(e);
        }
    }

    @Override
    public List<File> getFiles(String filename) throws DataException {
        ArrayList<File> queryResult = new ArrayList<File>();
        try {
            // Prepare Statement
            getFilesStatement.setString(1, filename);

            // Query result
            ResultSet rs = getFilesStatement.executeQuery();
            while (rs.next()) {
                int version = rs.getInt("version");
                int raidlevel = rs.getInt("raidlevel");
                Calendar cal = Calendar.getInstance();
                Date creation_date = rs.getTimestamp("creation_date");
                cal.setTime(creation_date);
                byte[] filehash = rs.getBytes("filehash");
                boolean marked = rs.getBoolean("marked");
                if (raidlevel == 1) {
                    queryResult.add(new File(filename, version, RaidLevel.Level1, cal, filehash, marked));
                } else {
                    queryResult.add(new File(filename, version, RaidLevel.Level5, cal, filehash, marked));
                }
            }
            return queryResult;
        } catch (SQLException e) {
            throw new DataException(e);
        }
    }

    @Override
    public List<File> getFiles() throws DataException {
        ArrayList<File> queryResult = new ArrayList<File>();
        try {
            // Query result
            ResultSet rs = getAllFilesStatement.executeQuery();
            while (rs.next()) {
                String filename = rs.getString("name");
                int version = rs.getInt("version");
                int raidlevel = rs.getInt("raidlevel");

                Calendar cal = Calendar.getInstance();
                Date creation_date = rs.getTimestamp("creation_date");
                cal.setTime(creation_date);

                byte[] filehash = rs.getBytes("filehash");
                boolean marked = rs.getBoolean("marked");
                if (raidlevel == 1) {
                    queryResult.add(new File(filename, version, RaidLevel.Level1, cal, filehash, marked));
                } else {
                    queryResult.add(new File(filename, version, RaidLevel.Level5, cal, filehash, marked));
                }
            }
            return queryResult;
        } catch (SQLException e) {
            throw new DataException(e);
        }
    }

    @Override
    public void putFile(File file) throws DataException {
        Calendar cal = Calendar.getInstance();
        cal.setTimeZone(TimeZone.getTimeZone("GMT"));

        try {
            if (file.getRaidLevel() == RaidLevel.Level1) {
                putFileStatementRaid1.setString(1, file.getName());
                putFileStatementRaid1.setInt(2, file.getVersion());
                putFileStatementRaid1.setInt(3, 1);
                putFileStatementRaid1.setBytes(5, file.getChecksum());
                putFileStatementRaid1.setTimestamp(4, new java.sql.Timestamp(cal.getTimeInMillis()));

                int rowsAffected = putFileStatementRaid1.executeUpdate();
                if (rowsAffected == 0) {
                    throw new DataException("Nothing inserted into database");
                }
            } else {
                putFileStatementRaid5.setString(1, file.getName());
                putFileStatementRaid5.setInt(2, file.getVersion());
                putFileStatementRaid5.setInt(3, 5);
                putFileStatementRaid5.setBytes(5, file.getChecksum());
                putFileStatementRaid5.setTimestamp(4, new java.sql.Timestamp(cal.getTimeInMillis()));

                int rowsAffected = putFileStatementRaid5.executeUpdate();
                if (rowsAffected == 0) {
                    throw new DataException("Nothing inserted into database");
                }
            }
        } catch (SQLException e) {
            throw new DataException(e);
        }
    }

    @Override
    public int getFileVersion(String filename) {
        try {
            getFilesLatestVersion.setString(1, filename);

            // Query result
            ResultSet rs = getFilesLatestVersion.executeQuery();
            if (rs.next())
                return rs.getInt(1);
            else
                return 0;

        } catch (SQLException e) {
            return 0;
        }
    }

    @Override
    public void deleteFile(String filename, int version) throws DataException {
        try {
            deleteFile.setString(1, filename);
            deleteFile.setInt(2, version);

            deleteFile.executeUpdate();
        } catch (SQLException e) {
            throw new DataException(e);
        }
    }

    @Override
    public void deleteFile(String filename) throws DataException {
        try {
            deleteFiles.setString(1, filename);

            deleteFiles.executeUpdate();
        } catch (SQLException e) {
            throw new DataException(e);
        }

    }

    @Override
    public void markFile(String filename, int version) throws DataException {
        try {
            // Prepare Statement
            updateFileStatement.setString(1, filename);
            updateFileStatement.setInt(2, version);

            int rowsAffected = updateFileStatement.executeUpdate();
            if (rowsAffected == 0) {
                throw new DataException("No file to marked into database");
            }
        } catch (SQLException e) {
            throw new DataException(e);
        }


    }
}
