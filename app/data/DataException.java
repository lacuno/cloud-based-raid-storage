package data;

/**
 * @author David
 */
public class DataException extends Exception {

    public DataException(String message) { super(message);}
    public DataException(Exception e) { super(e); }
}
