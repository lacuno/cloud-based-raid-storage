package data;

import data.model.FilePart;

import java.util.List;

/**
 * @author ivan
 */
public interface IFilePartsController {

    /**
     * getAllFileNames get all file names
     *
     * @return list of file names
     */
    public List<String> getAllFileNames() throws DataException;

    /**
     * getAllFileNamesFromStorageProvider get all file names from a storage provider
     *
     * @param  storageProviderId
     */
    public List<String> getAllFileNamesFromStorageProvider(String storageProviderId) throws DataException;

    /**
     * getCheckSum get checksum
     *
     * @param  filename
     * @param fileVersion
     */
    public String getCheckSum(String filename, int fileVersion, String storageProviderId) throws DataException;

    /**
     * getIsParity check if it is parity
     *
     * @param  filename
     * @param fileVersion
     * @param storageProviderId
     */
    public boolean getIsParity(String filename, int fileVersion, String storageProviderId) throws DataException;

    /**
     * getOrder get order
     *
     * @param  filename
     * @param fileVersion
     * @param storageProviderId
     */
    public int getOrder(String filename, int fileVersion, String storageProviderId) throws DataException;


    /**
     * putFilePart put File part
     *
     * @param filename
     * @param fileVersion
     * @param storageProviderId
     * @param checksum
     * @param isParity
     * @param order
     */
    public void putFilePart(String filename, int fileVersion, String storageProviderId, byte[] checksum, boolean isParity, int order) throws DataException;

    /**
     * getFileparts get a list os the file parts
     *
     * @param filename
     * @param fileVersion
     * @return list of file parts
     */
    public List<FilePart> getFileparts(String filename, int fileVersion) throws DataException;

    /**
     * deleteFilePart delete file parts by version
     *
     * @param filename
     * @param fileVersion
     */
    public void deleteFilePart(String filename, int fileVersion) throws DataException;

    /**
     * deleteFilePart delete file parts by filename
     *
     * @param filename
     */
    public void deleteFileParts(String filename) throws DataException;
}
