package data;

import data.model.File;

import java.util.List;

/**
 * @author David.
 * @author Christoph
 */
public interface IFileController {

    /**
     * getFile returns a single file that matches to filename and version
     *
     * @param filename
     * @param version
     * @return model.File file
     */
    public data.model.File getFile(String filename, int version) throws DataException;

    /**
     * getFiles returns a list of files matching to filename
     *
     * @param filename
     * @return list of model.File matching filename
     */
    public List<File> getFiles(String filename) throws DataException;

    /**
     * getFiles returns a list fo all files
     *
     * @return list of model.File
     */
    public List<data.model.File> getFiles() throws DataException;


    /**
     * putFile stores the given file in the database
     *
     * @param file file to store in the database
     * @throws DataException
     */
    public void putFile(File file) throws DataException;

    /**
     * markFile file as irreparable
     *
     * @param filename file to store in the database
     * @throws DataException
     */
    public void markFile(String filename, int version) throws DataException;

    /**
     * returns the latest version of the file
     *
     * @param filename
     * @return versionnumber of the latest file
     */
    public int getFileVersion(String filename);

    /**
     * deletes the file in the given version
     *
     * @param filename
     * @param version
     */
    public void deleteFile(String filename, int version) throws DataException;

    /**
     * deletes the file in the given filename
     *
     * @param filename
     */
    public void deleteFile(String filename) throws DataException;

}
