package data.model;

/**
 * @author ivan.
 */
public class FilePart implements  Comparable<FilePart> {
    private String fileName;
    private String storageProviderId;
    private byte[] checksum;
    private boolean isParity;

    /***
     * File constructor
     * @param fileName
     * @param storageProviderId
     * @param checksum
     * @param isParity
     * @param order
     *
     */
    public FilePart(String fileName, String storageProviderId, byte[] checksum, boolean isParity, int order) {
        this.fileName = fileName;
        this.storageProviderId = storageProviderId;
        this.checksum = checksum;
        this.isParity = isParity;
        this.order = order;
    }

    /***
     * setOrder set order
     * @param  order
     */
    public void setOrder(int order) {
        this.order = order;
    }

    private int order;

    /***
     * getVersion get file version
     * @return _name
     */
    public String getFileName() {
        return fileName;
    }

    /***
     * setName set file name
     * @param  fileName
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /***
     * getStorageProviderId get storage provider ID
     * @return  storageProviderId
     */
    public String getStorageProviderId() {
        return storageProviderId;
    }

    /***
     * setStorageProviderId set storage provider id
     * @param storageProviderId
     */
    public void setStorageProviderId(String storageProviderId) {
        this.storageProviderId = storageProviderId;
    }

    /***
     * getChecksum get checksum of a file
     * @return _checksum
     */
    public byte[] getChecksum() {
        return checksum;
    }

    /***
     * setChecksum get raid level
     * @param  checksum
     */
    public void setChecksum(byte[] checksum) {
        this.checksum = checksum;
    }

    /***
     * isParity check is it is parity
     * @return isParity
     */
    public boolean isParity() {
        return isParity;
    }

    /***
     * setParity set parity
     * @param isParity
     */
    public void setParity(boolean isParity) {
        this.isParity = isParity;
    }

    /***
     * getOrder return order
     * @return order
     */
    public int getOrder() {
        return order;
    }

    @Override
    public int compareTo(FilePart o) {
        if(this.order < o.order) {
            return -1;
        } else {
            return 1;
        }
    }
}
