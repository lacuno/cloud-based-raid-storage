package data.model;


import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author  David .
 */
public class File {

    private String _name;

    private int _version;

    private raid.RaidLevel _raidLevel;

    private Calendar _creation_date;

    private byte[] _checksum;

    private boolean _marked;

    private List<FilePart> _fileParts;

    /***
     * File constructor
     * @param name
     * @param version
     * @param raidLevel
     * @param creation_date
     * @param checksum
     * @param marked
     *
     */
    public File(String name, int version, raid.RaidLevel raidLevel, Calendar creation_date, byte[] checksum, boolean marked) {
        this._name = name;
        this._version = version;
        this._raidLevel = raidLevel;
        this._creation_date = creation_date;
        this._checksum = checksum;
        this._marked = marked;
    }

    /***
     * File constructor
     * @param name
     * @param version
     * @param raidLevel
     * @param creation_date
     * @param marked
     *
     */
    public File(String name, int version, raid.RaidLevel raidLevel, Calendar creation_date, boolean marked) {
        this._name = name;
        this._version = version;
        this._raidLevel = raidLevel;
        this._creation_date = creation_date;
        this._marked = marked;
    }

    /***
     * getName returns file name
     * @return _name
     */
    public String getName() {
        return _name;
    }

    /***
     * setName set file name
     * @param  name
     */
    public void setName(String name) {
        this._name = name;
    }


    /***
     * getVersion get file version
     * @return _name
     */
    public int getVersion() {
        return _version;
    }

    /***
     * setVersion set file version
     * @param version
     */
    public void setVersion(int version) {
        this._version = version;
    }

    /***
     * getRaidLevel get raid level
     * @return _raidLevel
     */
    public raid.RaidLevel getRaidLevel() {
        return _raidLevel;
    }

    /***
     * getRaidLevel set raid level
     * @param  raidLevel
     */
    public void setRaidLevel(raid.RaidLevel raidLevel) {
        this._raidLevel = raidLevel;
    }

    /***
     * getCreationDate get creation data
     * @return _creation_date
     */
    public Calendar getCreationDate() {
        return _creation_date;
    }


    /***
     * setCreationDate create date
     * @param _creation_date
     */
    public void setCreationDate(Calendar _creation_date) {
        this._creation_date = _creation_date;
    }

    /***
     * getChecksum get checksum of a file
     * @return _checksum
     */
    public byte[] getChecksum() {
        return _checksum;
    }

    /***
     * setChecksum get raid level
     * @param  data
     */
    public void setChecksum(byte[] data) {
        this._checksum = generateHash(data);
    }


    /***
     * getFileParts get file parts
     * @return _FileParts
     */
    public List<FilePart> getFileParts() {
        return _fileParts;
    }

    /***
     * setFileParts set file Parts
     * @param fileParts
     */
    public void setFileParts(List<FilePart> fileParts) {
        this._fileParts = fileParts;
    }

    /***
     * isMarkerd check is a file is markes
     * @return _marked
     */
    public boolean isMarked() {
        return _marked;
    }

    /***
     * setMarked mark a file
     * @param  _marked
     */
    public void setMarked(boolean _marked) {
        this._marked = _marked;
    }

    /***
     * getFullFilename get full file
     * @param
     */
    public String getFullFilename() {
        return this.getName() + "." + this.getVersion();
    }

    /***
     * generateHash  generate hash for a specific file
     * @param  data
     */
    public static byte[] generateHash(byte[] data) {
        if (data == null) {
            return null;
        }
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(data);
            return md.digest();
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }
}
