package data;

import com.google.inject.Singleton;
import controllers.File;
import data.model.FilePart;
import play.db.DB;

import javax.inject.Inject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ivan
 */
@Singleton
public class FilePartsController implements IFilePartsController {

    private Connection connection;
    private PreparedStatement pStatement;

    private static final String GET_ALL = "SELECT * FROM fileparts";
    private static final String GET_ALL_FROM_STORAGEPROVIDER = "SELECT * FROM fileparts WHERE storageProviderId = ?";
    private static final String GET_CHECKSUM = "SELECT checksum FROM fileparts WHERE fileName = ? AND fileVersion = ? AND storageProviderId = ?";
    private static final String GET_PARITY = "SELECT isParity FROM fileparts WHERE fileName = ? AND fileVersion = ? AND storageProviderId = ?";
    private static final String GET_ORDER = "SELECT partOrder FROM fileparts WHERE fileName = ? AND fileVersion = ? AND storageProviderId = ?";

    private static final String PUT_FILEPART = "INSERT INTO fileparts" +
            " (fileName, fileVersion, storageProviderId, checksum, isParity, partOrder) VALUES" +
            " (?, ?, ?, ?, ?, ?)";

    private static final String DELETE_FILEPART = "DELETE FROM fileparts WHERE fileName = ? and fileVersion = ?";
    private static final String DELETE_FILEPARTS = "DELETE FROM fileparts WHERE fileName = ?";

    private static final String GET_FILEPARTS = "SELECT * FROM fileparts WHERE fileName = ? AND fileVersion = ?";

    @Inject
    public FilePartsController() {
        connection = DB.getConnection();
    }


    @Override
    public List<String> getAllFileNames() throws DataException {
        List<String> filenames = new ArrayList<String>();
        try {
            pStatement = connection.prepareStatement(GET_ALL);
            ResultSet rs = pStatement.executeQuery();
            while (rs.next()) {
                filenames.add(rs.getString("fileName"));
            }
        } catch (SQLException e) {
            throw new DataException(e);
        }
        return filenames;
    }

    @Override
    public List<String> getAllFileNamesFromStorageProvider(String storageProviderId) throws DataException {
        List<String> filenames = new ArrayList<String>();
        try {
            pStatement = connection.prepareStatement(GET_ALL_FROM_STORAGEPROVIDER);
            pStatement.setString(1, storageProviderId);

            ResultSet rs = pStatement.executeQuery();
            while (rs.next()) {
                filenames.add(rs.getString("fileName"));
            }
        } catch (SQLException e) {
            throw new DataException(e);
        }
        return filenames;
    }

    @Override
    public String getCheckSum(String filename, int fileVersion, String storageProviderId) throws DataException {
        String checksum = "";
        try {
            pStatement = connection.prepareStatement(GET_CHECKSUM);
            pStatement.setString(1, filename);
            pStatement.setInt(2, fileVersion);
            pStatement.setString(3, storageProviderId);

            ResultSet rs = pStatement.executeQuery();
            if (rs.next()) {
                checksum = rs.getString("checksum");
            }
        } catch (SQLException e) {
            throw new DataException(e);
        }
        return checksum;
    }

    @Override
    public boolean getIsParity(String filename, int fileVersion, String storageProviderId) throws DataException {
        boolean parity = false;
        try {
            pStatement = connection.prepareStatement(GET_PARITY);
            pStatement.setString(1, filename);
            pStatement.setInt(2, fileVersion);
            pStatement.setString(3, storageProviderId);

            ResultSet rs = pStatement.executeQuery();
            if (rs.next()) {
                parity = rs.getBoolean("isParity");
            }
        } catch (SQLException e) {
            throw new DataException(e);
        }
        return parity;
    }

    @Override
    public int getOrder(String filename, int fileVersion, String storageProviderId) throws DataException {
        int order = -1;
        try {
            pStatement = connection.prepareStatement(GET_ORDER);
            pStatement.setString(1, filename);
            pStatement.setInt(2, fileVersion);
            pStatement.setString(3, storageProviderId);

            ResultSet rs = pStatement.executeQuery();
            if (rs.next()) {
                order = rs.getInt("order");
            }
        } catch (SQLException e) {
            throw new DataException(e);
        }
        return order;
    }

    @Override
    public void putFilePart(String filename, int fileVersion, String storageProviderId, byte[] checksum, boolean isParity, int order) throws DataException {
        try {
            pStatement = connection.prepareStatement(PUT_FILEPART);
            pStatement.setString(1, filename);
            pStatement.setInt(2, fileVersion);
            pStatement.setString(3, storageProviderId);
            pStatement.setBytes(4, checksum);
            pStatement.setBoolean(5, isParity);
            if(order < 0) pStatement.setNull(6, java.sql.Types.INTEGER);
            else pStatement.setInt(6, order);

            int rowsAffected = pStatement.executeUpdate();
            if (rowsAffected == 0) {
                throw new DataException("Nothing inserted into database");
            }

        } catch (SQLException e) {
            throw new DataException(e);
        }
    }

    @Override
    public List<FilePart> getFileparts(String filename, int fileVersion) throws DataException {
        List<FilePart> fileparts = new ArrayList<FilePart>();

        try {
            pStatement = connection.prepareStatement(GET_FILEPARTS);
            pStatement.setString(1, filename);
            pStatement.setInt(2, fileVersion);

            ResultSet rs = pStatement.executeQuery();

            while(rs.next()) {
                String fileName = rs.getString("fileName");
                String storageProviderId = rs.getString("storageProviderId");
                byte[] checksum = rs.getBytes("checksum");
                boolean parity = rs.getBoolean("isParity");
                int order = rs.getInt("partOrder");
                FilePart filePart = new FilePart(fileName, storageProviderId, checksum, parity, order);
                fileparts.add(filePart);
            }
            return fileparts;
        } catch (SQLException e) {
            throw new DataException(e);
        }
    }

    @Override
    public void deleteFilePart(String filename, int fileVersion) throws DataException {
        try {
            pStatement = connection.prepareStatement(DELETE_FILEPART);
            pStatement.setString(1, filename);
            pStatement.setInt(2, fileVersion);

            int rowsAffected = pStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DataException(e);
        }

    }

    @Override
    public void deleteFileParts(String filename) throws DataException {
        try {
            pStatement = connection.prepareStatement(DELETE_FILEPARTS);
            pStatement.setString(1, filename);

            int rowsAffected = pStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DataException(e);
        }
    }
}
