/**
 * Created by David on 15.12.2014.
 */

var serviceBasePath = "/file";

cloudraidApp.factory('RaidService', ['$http', '$upload', function ($http, $upload) {

    return {

        upload: function (filename, raidLevel, file) {
            return $upload.upload({
                method: "POST",
                url: serviceBasePath + "/" + filename + "/" + raidLevel,
                file: file
            });
        },

        list: function () {
            return $http.get(serviceBasePath + "/");
        },

        versions: function (filename) {
            return $http.get(serviceBasePath + "/" + filename);
        },

        download: function (filename, version) {
            return $http.get(serviceBasePath + "/" + filename + "/" + version);
        },

        delete: function (filename, version) {
            return $http.delete(serviceBasePath + "/" + filename + "/" + version);
        },

        deleteFile: function (filename) {
            return $http.delete(serviceBasePath + "/" + filename);
        },

        status: function () {
            return $http.get("/status");
        }

    }

}]);

cloudraidApp.factory('Filter', [function () {

    var filter = {value: null, order: null};

    return {

        setFilterValue: function (filterVal) {
            filter.value = filterVal;
        },

        setOrder: function (order) {
            if (filter.order && filter.order == order) {
                filter.order = "-" + filter.order;
            } else {
                filter.order = order;
            }
        },

        getFilter: function () {
            return filter;
        }

    }

}])