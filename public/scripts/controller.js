/**
 * Created by David on 15.12.2014.
 */

cloudraidApp.controller('FileListCtrl', ['$scope', '$timeout', '$location', 'Filter', 'RaidService', function ($scope, $timeout, $location, filterService, raidService) {

    $scope.currentPage = 1;

    $scope.filter = filterService.getFilter();

    raidService.list().then(function (payload) {
        $scope.files = payload.data;
        console.log($scope.files);
    });

    $scope.reload = function () {
        console.log("reloading");
        raidService.list().then(function (payload) {
            console.log("count: " + payload.data.length);
            $scope.files = payload.data;
        });
    };

    $scope.orderBy = function (field) {
        filterService.setOrder(field);
    };

    $scope.deleteFile = function (file) {
        console.log("file " + file.name + "." + file.version + " should be deleted now!");
        raidService.deleteFile(file.name).then(function (response) {

            $scope.successMessage = "File successfully deleted.";
            if (file.version == 1)
                $scope.files.splice($scope.files.indexOf(file), 1);
            else {
                $scope.reload();
            }

            $timeout(function () {
                $scope.successMessage = null;
            }, 5000);

        }, function (response) {

            console.log(response)

            $scope.errorMessage = "File couldn't be deleted. Message: " + response.data;
            $timeout(function () {
                $scope.errorMessage = null;
            }, 5000);

        });
    }

}]);

cloudraidApp.controller('FileDetailCtrl', ['$scope', '$timeout', '$routeParams', '$location', 'RaidService', function ($scope, $timeout, $routeParams, $location, raidService) {

    var filename = $routeParams.filename;
    console.log(filename);

    raidService.versions(filename).then(function (payload) {
        $scope.fileversions = payload.data;
        console.log($scope.fileversions);

        $scope.selectedFileVersion = $scope.fileversions[0];
    });

    $scope.setSelectedFileVersion = function (fileversion) {
        $scope.selectedFileVersion = fileversion;
    };

    $scope.deleteFile = function (file) {
        raidService.delete(file.name, file.version).then(function (response) {

            $scope.successMessage = "File successfully deleted.";
            $scope.fileversions.splice($scope.fileversions.indexOf(file), 1);

            if ($scope.fileversions.length > 0) {
                $scope.selectedFileVersion = $scope.fileversions[0];
            }
            else {
                $location.path("#/files");
            }

            $timeout(function () {
                $scope.successMessage = null;
            }, 5000);

        }, function (response) {

            console.log(response)

            $scope.errorMessage = "File couldn't be deleted. Message: " + response.data;
            $timeout(function () {
                $scope.errorMessage = null;
            }, 5000);

        });
    };

}]);

cloudraidApp.controller('FileUploadCtrl', ['$scope', 'RaidService', function ($scope, raidService) {

    $scope.raidLevel = 'Level1';
    $scope.file = null;
    $scope.filename = null;

    $scope.successMessage = null;
    $scope.errorMessage = null;
    $scope.progress = null;

    $scope.fileChanged = function (files, event) {
        console.log(files);
        if (files && files.length == 1) {
            if (!$scope.filename) {
                $scope.filename = files[0].name;
            }
        }
    };

    $scope.submitForm = function () {
        if ($scope.filename && $scope.raidLevel && $scope.file && $scope.file.length == 1) {
            raidService.upload($scope.filename, $scope.raidLevel, $scope.file[0]).progress(function (evt) {
                $scope.progress = parseInt(100.0 * evt.loaded / evt.total);
            }).success(function () {
                $scope.progress = null;
                $scope.filename = null;
                $scope.file = null;
                $scope.successMessage = ["File upload complete!"];
            }).error(function (data) {
                console.log(data);

                $scope.progress = null;
                $scope.errorMessage = ["File upload error."];
                $scope.errorMessage.push(data);
            });
        } else {
            $scope.errorMessage = ["Error at form validation.", "Please fill out all text fields, select an raid level and a file to upload."];
        }
    }

}]);

cloudraidApp.controller('ServerStatusCtrl', ['$scope', 'RaidService', function ($scope, raidService) {

    $scope.loading = true;

    raidService.status().then(function (response) {
        $scope.storageProviderStates = response.data;

        $scope.loading = false;
    }, function (response) {

        console.log(response);
        $scope.errorMessage = ["Couldn't receive data from server.", response.data];
        $scope.loading = false;

    })

}]);

cloudraidApp.controller('SearchCtrl', ['$scope', '$location', 'Filter', function ($scope, $location, filterService) {

    $scope.filterVal = "";

    $scope.$watch('filterVal', function (value) {
            filterService.setFilterValue(value);
        }
    );

    $scope.submitForm = function () {
        if ($location.path() != "/files") {
            $location.path("/files");
        }
    }

}]);

cloudraidApp.controller('RoutingCtrl', ['$scope', '$location', function ($scope, $location) {

    $scope.currentPage = "/";

    $scope.$on('$routeChangeSuccess', function () {
        $scope.currentPage = $location.path();
    });

}]);