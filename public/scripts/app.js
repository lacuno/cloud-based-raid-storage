/**
 * Created by David on 15.12.2014.
 */

var appBasePath = "/assets/";

var cloudraidApp = angular.module('cloudraidApp', ['ngRoute', 'ui.bootstrap', 'angularFileUpload']);

cloudraidApp.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider.
            when('/', {
                templateUrl: appBasePath + 'partials/home.html'
            }).
            when('/files', {
                templateUrl: appBasePath + 'partials/file-list.html',
                controller: 'FileListCtrl'
            }).
            when('/files/upload', {
                templateUrl: appBasePath + 'partials/file-upload.html',
                controller: 'FileUploadCtrl'
            }).
            when('/files/:filename', {
                templateUrl: appBasePath + 'partials/file-detail.html',
                controller: 'FileDetailCtrl'
            }).
            when('/status', {
                templateUrl: appBasePath + 'partials/server-status.html',
                controller: 'ServerStatusCtrl'
            }).
            otherwise({
                redirectTo: '/files'
            });
    }]);

cloudraidApp.filter('pagination', function () {
    return function (input, page, length) {
        if (input) {
            page = page - 1;
            return input.slice(length * page, length * (page + 1));
        }
    }
});

cloudraidApp.filter('filesize', function () {
    var units = [
        'bytes',
        'KB',
        'MB',
        'GB',
        'TB',
        'PB'
    ];

    return function (bytes, precision) {
        if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) {
            return '?';
        }

        var unit = 0;

        while (bytes >= 1024) {
            bytes /= 1024;
            unit++;
        }

        return bytes.toFixed(+precision) + ' ' + units[unit];
    };
});

cloudraidApp.filter('encodeUri', ['$window',
    function ($window) {
        return $window.encodeURIComponent;
    }
]);

